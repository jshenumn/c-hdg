CC = g++ -Wall
default: test
OBJS = Mesh.o Quadrature.o Basis_function.o Physics.o Collocation.o Local_solver.o Global_solver.o Reconstruction.o Postprocessing.o
EIGEN_PATH = ./Eigen
test: $(OBJS) main.o  
	$(CC) -o driver.exe main.o $(OBJS) -I$(EIGEN_PATH)
clean:
	rm *.o *.mod *.*~ driver.exe

clean-data:
	cd DATA; rm *.dat; cd ..

.SUFFIXES: .cpp .o
.cpp.o:
	$(CC) -c $< -I$(EIGEN_PATH)
