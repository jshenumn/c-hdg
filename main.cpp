#include<iostream>
#include<vector>
#include<cmath>
#include<fstream>
#include<sstream>
#include"Basis_function.h"
#include"Physics.h"
#include"Mesh.h"
#include"Collocation.h"
#include"Quadrature.h"
#include"Local_solver.h"
#include"Global_solver.h"
#include"Reconstruction.h"
#include"Postprocessing.h"
#include<Eigen/Eigen>
#include<Eigen/Sparse>
//#include<Eigen/IterativeSolvers>
#include<time.h>
#include<stdio.h>
#include<math.h>

using namespace Eigen;
using namespace std;


int main(){
    
    // user interface
    cout << "Welcome to HDG method for 2D steady-state diffusion equation" << endl;
    cout << "Please input the initial mesh level (1-8)" << endl;
    
    int level = 1;
    int poly_degree = 1;
    cin >> level;
    cout << "Please input polynomial degree (1-5)" << endl;
    cin >> poly_degree;
    
    //error U,Q initialization
    double eoldU = 0.0;
    double eoldQ = 0.0;
    double eoldUstar = 0.0;
    int nhoc = 1;
    
    
    /*predefined values*/
    
    int dim_m = poly_degree+1;
    int dim_w = (poly_degree+1)*(poly_degree+2)/2;
    int dim_v = (poly_degree+1)*(poly_degree+2);
    int dim_wstar = (poly_degree+2)*(poly_degree+3)/2;
    
    Quad QE;
    QE.set_edge_quad(2*poly_degree+2);
    int n_pt = QE.get_n_pt();
    Quad QT;
    QT.set_ele_quad(2*poly_degree+2);
    int m_pt = QT.get_n_pt();
    /*predefined values*/
    
    
    /*-------------------------MAIN LOOP---------------------------------------*/
    while (level <= 9) {
     
        
    double orderU = 0.0;
    double orderQ = 0.0;
    double orderUstar = 0.0;
    clock_t t,t2,t3,t4,t5,t6,t7,t8;
    t = clock(); //track CPU time
    
    //---------------- collocation ---------------------------------------------
    double col_m_edge[dim_m*n_pt];
    double col_w_edge[dim_w*n_pt][3];
    double col_v_edge[dim_v*n_pt][6];
    
    double col_w_triangle[dim_w*m_pt];
    double col_v_triangle[dim_v*m_pt][2];
    double col_jv_triangle[dim_v*m_pt][4];
    double col_wstar_triangle[dim_wstar*m_pt];
    
    collocation_edge(dim_m, dim_w, dim_v, col_m_edge, col_w_edge, col_v_edge, QE);
    collocation_triangle(dim_w, dim_v, col_w_triangle, col_jv_triangle, col_v_triangle, QT);
    
    //---------------- initializate mesh ---------------------------------------
    string path_ele = "./MESH/MESH."+ to_string(level) + ".ele";
    string path_edge = "./MESH/MESH."+ to_string(level) + ".edge";
    string path_node = "./MESH/MESH."+ to_string(level) + ".node";
    
    //---------------- read files from directory -------------------------------
    t5 = clock();
    vector<Row> element_table = mesh_read(path_ele);
    vector<Row> edge_table = mesh_read(path_edge);
    vector<Row> node_table = mesh_read(path_node);
    t5 = clock()-t5;
    
    //---------------- initialize mesh containers ------------------------------
    int n_ele = 0;
    int n_edge = 0;
    Element2D * element_list = new Element2D[element_table.size()];
    Point2D * node_list = new Point2D[node_table.size()];
    Edge2D *edge_list = new Edge2D[edge_table.size()];
    vector<int> DBCarray;
    
    //------------ process the mesh,compute geometry -------------------------
    t4 = clock();
    make_mesh(n_ele, n_edge, element_list, edge_list, node_list, DBCarray, element_table, edge_table, node_table);
    t4 = clock()-t4;
    
    
    //========================== LOCAL SOLVER ===============================
    Local_Mat * Lmat_list = new Local_Mat[n_ele]; //local solver object array
    t3 = clock();
        
    for (int i = 1; i <= n_ele; i++) { // loop over all element

        compute_local_solver_matrix(element_list[i], edge_list, node_list, source_f, col_v_triangle, col_jv_triangle, col_w_triangle, col_m_edge, col_w_edge,col_v_edge, dim_m, dim_w, dim_v, QT, QE, Lmat_list[i-1]);
        
    }
    t3 = clock() - t3;
    
        
    //=========================  Global Assemble =============================
    
    int Mymap[n_edge][6];
    for (int i=0; i < n_edge; ++i){
        for (int j= 1; j < 6; ++j){
            Mymap[i][j] = -1;
        }
        Mymap[i][0] = 0;
    }
    SpMat A(dim_m*n_edge,dim_m*n_edge); //sparse matrix initialization
    A.reserve(VectorXd::Constant(dim_m*n_edge,5*dim_m)); //! key for high performance reserve maximum     nonzeros per row (maximum 5 blocks due to HDG method!)
    VectorXd rhs(dim_m*n_edge);
    rhs.setZero();
    
    t6 = clock();
    Global_Assemble(A, rhs, Lmat_list, Mymap, n_ele, n_edge, element_list, dim_m);
    dirichletBC(A, rhs, Mymap, DBCarray, edge_list, node_list, col_m_edge, dim_m, boundary_g, QE);
    t6 = clock()-t6;
        
    //========================= Global Solver ===============================
    t2 = clock();
    // conjugate gradient with incomplete LU preconditioner
    ConjugateGradient<SparseMatrix<double>, 1, SimplicialCholesky<SparseMatrix<double> > > cg;
    cg.compute(A);
    VectorXd solUhat(dim_m*n_edge);
    solUhat.setZero();
    solUhat = cg.solve(rhs);
    t2 = clock() - t2;
    
    
    //========================= Reconstruction ==============================
    VectorXd solU(dim_w*n_ele); solU.setZero();
    VectorXd solQ(dim_v*n_ele); solQ.setZero();
    
    t8 = clock();
    reconstruction(solUhat, solU, solQ, element_list, Lmat_list, dim_m, dim_w, dim_v, n_ele);
    t8 = clock()-t8;
        
        
        
    //========================= Postprocessing ==============================
    t7 = clock();
    VectorXd solpU(dim_wstar*n_ele); solpU.setZero();
        
        
    /*additional collocation for postprocessing*/
    int n_pt = QT.get_n_pt();
    double * wts = new  double[n_pt];
    Point2D * pts = new Point2D[n_pt];
    
    wts = QT.get_wts();
    pts = QT.get_pts();
    for (int j = 0; j < m_pt; ++j ) {
        for (int i = 0; i < dim_wstar; ++i) {
            col_wstar_triangle[i + j*dim_wstar] = basis_triangle(i+1,pts[j].xcord(),pts[j].ycord()); // store in a vector using vectorization convention.
        }
    }
        
    for (int ii = 1; ii <= n_ele; ii++) { // loop over all element
        // define local portion to modify or use
        int gindv = (ii-1)*dim_v;
        int gindw = (ii-1)*dim_w;
        int gindpw = (ii-1)*dim_wstar;
        
        VectorXd loc_solQ(dim_v); loc_solQ.setZero(); loc_solQ = solQ.segment(gindv,dim_v);
        VectorXd loc_solpU(dim_wstar); loc_solpU.setZero(); loc_solpU = solpU.segment(gindpw,dim_wstar);
        double avgU = 0.0; avgU = solU(gindw);
        
        postprocessing(loc_solpU, loc_solQ, avgU, element_list[ii], edge_list, dim_v,dim_wstar,  col_wstar_triangle,col_v_triangle, QT);
        
        // put back
        solpU.segment(gindpw,dim_wstar) = loc_solpU;
        }
    t7 = clock()-t7;
        
    t = clock() - t;
    //========================= Validation ==================================
    double L2errQ = 0.0;
    double L2errU = 0.0;
    double L2errUstar = 0.0;
    
    validation(L2errQ, L2errU, L2errUstar, solU, solQ, solpU, exactU, exactQ, n_ele, element_list, node_list, col_v_triangle,col_w_triangle,QT, dim_v, dim_w, dim_wstar);
    
        
    //========================= Compute order of convergence =================
      if (nhoc > 1) { // first cannot have order of convergence
          orderQ = -log2(L2errQ/eoldQ)/log2(2.0);
          orderU = -log2(L2errU/eoldU)/log2(2.0);
          orderUstar = -log2(L2errUstar/eoldUstar)/log2(2.0);
      }
    eoldQ = L2errQ;
    eoldU = L2errU;
    eoldUstar = L2errUstar;
        
    
    //========================= Result Print =================================
    timestamp();
    cout <<"=========== Infomation ========" << endl;
    cout.width(30);cout <<left<<"The total number of elements: " << right << n_ele << endl;
    cout.width(30);cout <<left<<"The total number of edges: " << right <<  n_edge << endl;
    cout.width(30);cout <<left<<"The size of stiffness matrix:" << right << A.cols() << endl;
    cout.width(30);cout <<left<<"The polynomial degree:" << right << poly_degree << endl;
    
    
    cout<<"=========== CPU ================" << endl;
    cout.width(40);cout <<left<<"The total CPU time(s): "<< right <<((float)t)/CLOCKS_PER_SEC << endl;
    cout.width(40);cout <<left<<"The CPU time(s) for mesh reading: "<<right<<((float)t5)/CLOCKS_PER_SEC <<"("<< int((float)t5/(float)t*100)  <<"%)"<< endl;
    cout.width(40);cout <<left<<"The CPU time(s) for mesh processing: "<<right<<((float)t4)/CLOCKS_PER_SEC<<"("<< int((float)t4/(float)t*100)  <<"%)" << endl;
    cout.width(40);cout <<left<<"The CPU time(s) for local solver:"<<right<<((float)t3)/CLOCKS_PER_SEC<<"("<< int((float)t3/(float)t*100) <<"%)" << endl;
    cout.width(40);cout <<left<<"The CPU time(s) for global assemble: "<<right<<((float)t6)/CLOCKS_PER_SEC <<"("<< int((float)t6/(float)t*100)  <<"%)"<< endl;
    cout.width(40);cout <<left<<"The CPU time(s) for global solver: "<<right<<((float)t2)/CLOCKS_PER_SEC <<"("<< int((float)t2/(float)t*100) <<"%)"<< endl;
    cout.width(40);cout <<left<<"The CPU time(s) for reconstruction: "<<right<<((float)t8)/CLOCKS_PER_SEC <<"("<< int((float)t8/(float)t*100) <<"%)"<< endl;
    cout.width(40);cout <<left<<"The CPU time(s) for postprocessing: "<<right<<((float)t7)/CLOCKS_PER_SEC <<"("<< int((float)t7/(float)t*100) <<"%)"<< endl;
    
    cout <<"============ ERROR ============" << endl;
    cout.width(40);cout << left << "The L2-error in u:" << L2errU << endl;
    cout.width(40);cout << left << "The L2-error in q:" << L2errQ << endl;
    cout.width(40);cout << left << "The L2-error in ustar:" << L2errUstar << endl;

    if (nhoc == 1) {
    cout.width(40);cout << left << "The order of convergence in u:" << "-" << endl;
    cout.width(40);cout << left << "The order of convergence in q:" << "-" << endl;
    cout.width(40);cout << left << "The order of convergence in ustar:" << "-" << endl;

    }
    else{
    cout.width(40);cout << left << "The order of convergence in u:" << orderU << endl;
    cout.width(40);cout << left << "The order of convergence in q:" << orderQ << endl;
    cout.width(40);cout << left << "The order of convergence in ustar:" << orderUstar << endl;

    }
    nhoc++;
    cout << endl;
    cout << endl;
        
    delete [] element_list;
    delete [] node_list;
    delete [] edge_list;
    delete [] Lmat_list;
        
    if (level != 9) {
    cout << "Do you want to refine? (Yes 1/ No 0)" << endl;
    int refind;
    cin >> refind;
        
        if (refind == 0) { //no refinement
            cout << "Thank you! Goodbye!" << endl;
            break;
        }
        else{
            if (refind == 1){ // refine
                level++;
            }
            else{
            cout << "Invalid input! Program exit!" << endl;
            }
        }
    }
    else{
        cout << "Reach maximum refinement! Program exit!" << endl;
        return 0;
        }
    }// while loop
}