#include<iostream>
#include<vector>
#include<cmath>
#include"Basis_function.h"
#include"Physics.h"
#include"Mesh.h"
#include"Collocation.h"
#include"Quadrature.h"
#include"Local_solver.h"
#include <Eigen/Eigen> // invoke Eigen routines for matrix

using namespace std;


// class implementation
Local_Mat::Local_Mat(){ // default constructor
    MatrixXd  KK(6,6); KK.setZero();
    MatrixXd  BB(6,6); BB.setZero();
    MatrixXd  Aq(6,6); Aq.setZero();
    MatrixXd  Au(6,6); Au.setZero();
    
    VectorXd  RHSq(6); RHSq.setZero();
    VectorXd  RHSu(3); RHSu.setZero();
    dim_m = 2;
    
    
}

Local_Mat::~Local_Mat(){ // overload constructor
}


Local_Mat & Local_Mat::operator= (const Local_Mat & other){
    return *this;
}



void Local_Mat::set_KK(MatrixXd &K){
     KK = K;
}

void Local_Mat::set_BB(VectorXd &B){
     BB = B;
}

void Local_Mat::set_Aq(MatrixXd &Q){
     Aq = Q;
}

void Local_Mat::set_Au(MatrixXd &U){
     Au = U;
}

void Local_Mat::set_RHSq(VectorXd &Rq){
     RHSq = Rq;
}

void Local_Mat::set_RHSu(VectorXd &Ru){
     RHSu = Ru;
}


// function prototypes


// output mtx_qv is Matrix double using Eigen package
void compute_qv(MatrixXd &mtx_qv, Element2D & ele, double col_v_triangle[][2], int dim_v, Quad &QT){
    
     // get quadrature info
     int n_pt = QT.get_n_pt();
     double * wts =  new double[n_pt];
     wts = QT.get_wts();
     double area = ele.get_area();
    
     // compute output
     for (int i = 0; i < dim_v; ++i) {
        for (int j = 0; j < dim_v; ++j) {
            double val = 0.0;
            for (int k = 0; k < n_pt; ++k) {
                val = val + area * wts[k] * (col_v_triangle[i + k*dim_v][0] * col_v_triangle[j + k*dim_v][0] + col_v_triangle[i + k*dim_v][1] * col_v_triangle[j + k*dim_v][1]);
            }
            mtx_qv(i,j) = val;
         }
     }
    
}


void compute_udivv(MatrixXd &mtx_udivv, Element2D &ele, Edge2D edge_list[], double col_jv_triangle[][4], double col_w_triangle[], int dim_v, int dim_w, Quad &QT){
   
    // get quad info
    int n_pt = QT.get_n_pt();
    double * wts =  new double[n_pt];
    wts = QT.get_wts();
    double area = ele.get_area();
    
    double divEle[dim_v*n_pt];
    div_R2(divEle, ele, edge_list, dim_v, n_pt, col_jv_triangle);
    
    // compute output
    for (int i = 0; i < dim_v; ++i) {
        for (int j = 0; j < dim_w; ++j) {
            double val = 0.0;
            for (int k = 0; k < n_pt; ++k) {
                val = val + area * wts[k]* divEle[i + k*dim_v]* col_w_triangle[j + k*dim_w];
            }
            mtx_udivv(i,j) = val;
        }
    }
    
    

}

void compute_fw(VectorXd &mtx_fw, Element2D &ele, Point2D node_list[], double col_w_triangle[], int dim_w , Quad &QT ,std::function<double(double,double)> func){
    
    // get integration info from quadrature on triangle
    int n_pt = QT.get_n_pt();
    double * wts = new double[n_pt];
    Point2D * pts = new Point2D[n_pt];
    
    wts = QT.get_wts();
    pts = QT.get_pts();
    
    // get info from element
    int * cnt =  new int[3]; // get vert global numbering
    cnt = ele.get_cnt();
    
    double area = ele.get_area();
    
    // compute output
    for (int i = 0; i < dim_w; ++i) {
        double val = 0.0;
        for (int k = 0; k < n_pt; ++k) {
            double l1 = pts[k].xcord();
            double l2 = pts[k].ycord();
            double X = l1*node_list[cnt[0]].xcord()  +  l2*node_list[cnt[1]].xcord() + (1-l1-l2)*node_list[cnt[2]].xcord();
            double Y = l1*node_list[cnt[0]].ycord()  +  l2*node_list[cnt[1]].ycord() + (1-l1-l2)*node_list[cnt[2]].ycord();
            val = val + area * wts[k]*col_w_triangle[i + k*dim_w]*func(X,Y);
        }
        mtx_fw[i] = val;
    }
    
}

void compute_uhatuhat(MatrixXd mtx_uhatuhat[], Element2D &ele, Edge2D edge_list[], double col_m_edge[], int dim_m, Quad &QE){
    
    // get integration pts
    int n_pt = QE.get_n_pt();
    double * wts = new double[n_pt];
    Point2D * pts = new Point2D[n_pt];
    wts = QE.get_wts();
    pts = QE.get_pts();
    
    // get tau
    double * tau = new double[3];
    tau = ele.get_tau();
    
    // compute output
    
    for (int ne = 1; ne <= 3; ++ne){ // local edge orientation is from 1 to 3
        // get orientation
        int ori = ele.get_ori(ne);
        int edge_id = ele.get_edg_gn(ne-1);
        double length = edge_list[edge_id].get_length();
        
        
        // check orientation
        for (int i = 0; i < dim_m; ++i) {
            for (int j = 0; j < dim_m; ++j) {
                double val = 0.0;
                for (int k = 0; k < n_pt; ++k) {
                    if (ori  == -1) { // need to flip weights
                        
                        val = val + length*tau[ne-1]*wts[n_pt-1-k]*col_m_edge[i + k*dim_m] * col_m_edge[j + k*dim_m];
                        
                    } else{
                        
                        val = val + length*tau[ne-1]*wts[k]*col_m_edge[i + k*dim_m] * col_m_edge[j + k*dim_m];
                    }
                    
                }// end for k
                mtx_uhatuhat[ne-1](i,j) = val;
            }
        }
        
        
        
        
        
        
    
    
    
    }// big kk loop
    
}

void compute_uhatvn(MatrixXd mtx_uhatvn[], Element2D &ele, Edge2D edge_list[], double col_v_edge[][6], double col_m_edge[], int dim_v, int dim_m, Quad &QE){
    
    // get integration pts
    int n_pt = QE.get_n_pt();
    double * wts = new double[n_pt];
    Point2D * pts = new Point2D[n_pt];
    double * nrm = new double[6];
    nrm  = ele.get_normal();
    
    wts = QE.get_wts();
    pts = QE.get_pts();
    
    
    for (int ne = 1; ne <= 3; ++ne){ // local edge orientation is from 1 to 3
        // get orientation
        int ori = ele.get_ori(ne);
        int edge_id = ele.get_edg_gn(ne-1);
        double length = edge_list[edge_id].get_length();
    
    // compute output
    for (int i = 0; i < dim_v; ++i) {
        for (int j = 0; j < dim_m; ++j) {
            double val = 0.0;
            for (int k = 0; k < n_pt ; ++k) {
                if (ori == -1) {
                    val = val + length*wts[n_pt-1-k]*col_m_edge[j + (n_pt-1-k)*dim_m]*(nrm[(ne-1)*2]*col_v_edge[i + k*dim_v][(ne-1)*2] + nrm[(ne-1)*2+1]*col_v_edge[i + k*dim_v][(ne-1)*2 + 1]);
                    }
                else{
                    val = val + length*wts[k]*col_m_edge[j + k*dim_m]*(nrm[(ne-1)*2]*col_v_edge[i + k*dim_v][(ne-1)*2] + nrm[(ne-1)*2+1]*col_v_edge[i + k*dim_v][(ne-1)*2 + 1]);
                    }
                }//end for k
            mtx_uhatvn[ne-1](i,j) = val;
            }
        }
    
    }
    
    
}

void compute_uhuh(MatrixXd &mtx_uhuh, Element2D &ele, Edge2D edge_list[], double col_w_edge[][3], int dim_w, Quad &QE){
    // get integration pts
    int n_pt = QE.get_n_pt();
    double * wts = new double[n_pt];
    Point2D * pts = new Point2D[n_pt];
    wts = QE.get_wts();
    pts = QE.get_pts();
    
    // get tau
    double * tau = new double[3];
    tau = ele.get_tau();
    
    
    // compute output
    for (int ne = 1; ne <= 3; ++ne){ // local edge orientation is from 1 to 3
        // get orientation
        int edge_id = ele.get_edg_gn(ne-1);
        double length = edge_list[edge_id].get_length();
    
    // construct output mat
    for (int i = 0; i < dim_w; i++) {
        for (int j = 0; j < dim_w; j++) {
            double val = 0.0;
            for (int k = 0; k < n_pt; k++) {
                //cout << "location" << i << j << "\t" << val << endl;
                val +=length*wts[k]* tau[ne-1] * col_w_edge[i + k*dim_w][ne-1]*col_w_edge[j + k*dim_w][ne-1];
                //cout << length << endl;
                }
            mtx_uhuh(i,j) += val;
            }
        }
    }
}

void compute_uhuhat(MatrixXd mtx_uhuhat[], Element2D &ele, Edge2D edge_list[], double col_m_edge[], double col_w_edge[][3], int dim_w, int dim_m, Quad &QE){
    
    // get integration pts
    int n_pt = QE.get_n_pt();
    double * wts = new double[n_pt];
    Point2D * pts = new Point2D[n_pt];
    wts = QE.get_wts();
    pts = QE.get_pts();
    
    // get tau
    double * tau = new double[3];
    tau = ele.get_tau();
    
    
    // compute output
    for (int ne = 1; ne <= 3; ++ne){ // local edge orientation is from 1 to 3
        // get orientation
        int ori = ele.get_ori(ne);
        int edge_id = ele.get_edg_gn(ne-1);
        double length = edge_list[edge_id].get_length();
    
    // constuct local matrix
        for (int i = 0; i < dim_w; ++i) {
            for (int j = 0; j < dim_m; ++j) {
                double val = 0.0;
                for (int k = 0; k < n_pt; ++k) {
                    if (ori == -1) {
                        val = val + length * tau[ne-1]* wts[k] * col_m_edge[j + (n_pt-1-k)*dim_m]*col_w_edge[i + k*dim_w][ne-1];
                    }
                    else{
                        val = val + length * tau[ne-1]* wts[k] * col_m_edge[j + k*dim_m]*col_w_edge[i + k*dim_w][ne-1];
                    }
                }// end for k
                mtx_uhuhat[ne-1](i,j) = val;
            }
        }

    }
    
}



void compute_local_solver_matrix(Element2D &ele, Edge2D edge_list[], Point2D node_list[], std::function<double(double,double)> func, double col_v_triangle[][2], double col_jv_triangle[][4], double col_w_triangle[], double col_m_edge[], double col_w_edge[][3], double col_v_edge[][6], int dim_m, int dim_w, int dim_v, Quad &QT, Quad &QE, Local_Mat & Lmat){
    
    // This a big function, the first part run local functions
    
    //inintialize local matrices
    MatrixXd mtx_qv(dim_v,dim_v);
    mtx_qv.setZero();
    
    
    MatrixXd mtx_udivv(dim_v,dim_w);
    mtx_udivv.setZero();
    
    
    VectorXd mtx_fw(dim_w);
    mtx_fw.setZero();
    
    
    MatrixXd mtx_uhatuhat[3];
    for(int i = 0; i < 3; i++){
        mtx_uhatuhat[i] = MatrixXd(dim_m,dim_m);
        mtx_uhatuhat[i].setZero();
    }
    
    MatrixXd mtx_uhatvn[3];
    for(int i = 0; i < 3; i++){
        mtx_uhatvn[i] = MatrixXd(dim_v,dim_m);
        mtx_uhatvn[i].setZero();
    }
    
    MatrixXd mtx_uhuh(dim_w,dim_w);
    mtx_uhuh.setZero();
    
    
    MatrixXd mtx_uhuhat[3];
    for(int i = 0; i < 3; i++){
        mtx_uhuhat[i] = MatrixXd(dim_w,dim_m);
        mtx_uhuhat[i].setZero();
    }

    
    
    // run local computating functions
    compute_qv(mtx_qv, ele, col_v_triangle, dim_v, QT);
    compute_udivv(mtx_udivv, ele, edge_list, col_jv_triangle, col_w_triangle, dim_v, dim_w, QT);
    compute_fw(mtx_fw, ele, node_list, col_w_triangle, dim_w, QT, source_f);
    compute_uhatuhat(mtx_uhatuhat, ele, edge_list, col_m_edge, dim_m, QE);
    compute_uhatvn(mtx_uhatvn, ele, edge_list, col_v_edge, col_m_edge, dim_v, dim_m, QE);
    compute_uhuh(mtx_uhuh, ele, edge_list, col_w_edge, dim_w, QE);
    compute_uhuhat(mtx_uhuhat, ele, edge_list, col_m_edge, col_w_edge, dim_w, dim_m, QE);
    
    
    // Local matrix A construction
    MatrixXd A(dim_v+dim_w,dim_v+dim_w);
    MatrixXd matzero(dim_v,dim_w);
    matzero.setZero();
    A.setZero();
    
    // assign local matrices
    A.topLeftCorner(dim_v,dim_v) = mtx_qv;
    A.topRightCorner(dim_v,dim_w) = matzero - mtx_udivv;
    A.bottomLeftCorner(dim_w,dim_v) = mtx_udivv.transpose();
    A.bottomRightCorner(dim_w,dim_w) = mtx_uhuh;
    
    // Local RHS construction (could be smarter, need to rewrite)
    VectorXd fvec(dim_v + dim_w);
    for (int  i = 0; i < dim_v; ++i) {
        fvec(i) = 0.0;
    }
    
    for (int  j = 0; j < dim_w; ++j) {
        fvec(j + dim_v) = 0.0 - mtx_fw(j);
    }
    
    // Local Uhat matrix construction
    MatrixXd Uhat(dim_v + dim_w, 3*dim_m);
    Uhat.block(0,0,dim_v,dim_m) = - mtx_uhatvn[0];
    Uhat.block(0,dim_m,dim_v,dim_m) = - mtx_uhatvn[1];
    Uhat.block(0,dim_m*2,dim_v,dim_m) = - mtx_uhatvn[2];
    
    Uhat.block(dim_v,0,dim_w,dim_m) = mtx_uhuhat[0];
    Uhat.block(dim_v,dim_m,dim_w,dim_m) = mtx_uhuhat[1];
    Uhat.block(dim_v,dim_m*2,dim_w,dim_m) = mtx_uhuhat[2];
    
    
    
    //compute inverse of local system
    MatrixXd Ae(dim_v + dim_w, 3*dim_m);
    VectorXd RHSe(dim_v + dim_w);
    
    Ae = A.inverse() * Uhat;
    RHSe = A.inverse() *fvec;
    
    // extract local portion for q and u respctively
    MatrixXd Aq(dim_v, 3*dim_m);
    MatrixXd Au(dim_w, 3*dim_m);
    
    VectorXd RHSq(dim_v);
    VectorXd RHSu(dim_w);
    
    Aq = Ae.block(0,0,dim_v,3*dim_m);
    Au = Ae.block(dim_v,0,dim_w,3*dim_m);
    
    RHSq = RHSe.segment(0,dim_v);
    RHSu = RHSe.segment(dim_v,dim_w);

    // Allocate local solver output
    MatrixXd KK(3*dim_m,3*dim_m);
    VectorXd BB(3*dim_m);
    VectorXd zerosB(3*dim_m);
    zerosB.setZero();
    
    MatrixXd proxy1(dim_v,3*dim_m);
    MatrixXd proxy2(dim_w,3*dim_m);
    proxy1= Uhat.block(0,0,dim_v,3*dim_m);
    proxy2= Uhat.block(dim_v,0,dim_w,3*dim_m);
    
    KK = -proxy1.transpose() *  Aq + proxy2.transpose() * Au;
    BB = -proxy1.transpose() *  RHSq + proxy2.transpose() * RHSu;
    
    // final modification on diagonal
    KK.block(0,0,dim_m,dim_m) = KK.block(0,0,dim_m,dim_m) - mtx_uhatuhat[0];
    KK.block(dim_m,dim_m,dim_m,dim_m) = KK.block(dim_m,dim_m,dim_m,dim_m) - mtx_uhatuhat[1];
    KK.block(2*dim_m,2*dim_m,dim_m,dim_m) = KK.block(2*dim_m,2*dim_m,dim_m,dim_m) - mtx_uhatuhat[2];
    
    
    
    // Modified the Lmat object
    BB = zerosB - BB;
    
    Lmat.set_KK(KK);
    Lmat.set_BB(BB);
    Lmat.set_Aq(Aq);
    Lmat.set_Au(Au);
    Lmat.set_RHSq(RHSq);
    Lmat.set_RHSu(RHSu);
    
}













