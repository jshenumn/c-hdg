#include<iostream>
#include<cmath>
#include"Physics.h"
using namespace std;


//double source_f(double x, double y){
//    return -6.0*(x-0.5) - 6.0*(y-0.5);
//}
//
//double boundary_g(double x, double y, int marker){
//    switch (marker) {
//        case 1: // 1st piece of boundary
//            return (x-0.5)*(x-0.5)*(x-0.5)+(y-0.5)*(y-0.5)*(y-0.5);
//            break;
//        case 2:
//            return (x-0.5)*(x-0.5)*(x-0.5)+(y-0.5)*(y-0.5)*(y-0.5);
//            break;
//        case 3:
//            return (x-0.5)*(x-0.5)*(x-0.5)+(y-0.5)*(y-0.5)*(y-0.5);
//            break;
//        case 4:
//            return (x-0.5)*(x-0.5)*(x-0.5)+(y-0.5)*(y-0.5)*(y-0.5);
//            break;
//        default:
//            return (x-0.5)*(x-0.5)*(x-0.5)+(y-0.5)*(y-0.5)*(y-0.5);
//            break;
//    }
//}
//
//
//double exactU(double x, double y){
//    return (x-0.5)*(x-0.5)*(x-0.5)+(y-0.5)*(y-0.5)*(y-0.5);
//}
//
//double * exactQ(double x, double y){
//    double * val = new double[2];
//    val[0] = -3*(x-0.5)*(x-0.5);
//    val[1] = -3*(y-0.5)*(y-0.5);
//    return val;
//    delete [] val;
//}





 double source_f( double x,  double y){
    return 2.0*sin(x)*cos(y);
}

 double boundary_g( double x,  double y, int marker){
    switch (marker) {
        case 1: // 1st piece of boundary
            return sin(x)*cos(y);
            break;
        case 2:
            return sin(x)*cos(y);
            break;
        case 3:
            return sin(x)*cos(y);
            break;
        case 4:
            return sin(x)*cos(y);
            break;
        default:
            return sin(x)*cos(y);
            break;
    }
}


 double exactU( double x,  double y){
    return sin(x)*cos(y);
}

 double * exactQ( double x, double y){
    double * val = new double[2];
    val[0] = -cos(x)*cos(y);
    val[1] = sin(x)*sin(y);
    return val;
    delete [] val;
}








