#ifndef QUADRATURE_H
#define QUADRATURE_H

#include "Mesh.h"

// class definition
class Quad{
public:
    Quad(); // default constructor
    //Quad(const Quad & qd); // copy constructor
    ~Quad(); // destructor
    
    //accessor
    int get_n_pt(){return n_pt;};
    double * get_wts(){return wts;};
    Point2D * get_pts(){return pts;};
    
    void set_edge_quad(int deg); // set quadrature on edge
    void set_ele_quad(int deg); // set quadrature on element
    
    
private:
    int n_pt;
    double *wts;
    Point2D *pts; // the pts has x,y coordinates so a Point type pointer
};

// functions prototypes:
void LGWT(int n_pt, double a , double b, double pts[], double wts[]);
int gm_rule_size(int rule, int dim_num); // gm rule size determination
int i4_choose(int n, int k); // simple choose k from n implementation
void comp_next (int n, int k, int a[], bool *more, int *h, int *t );
void gm_rule_set (int rule, int dim_num, int point_num, double pts[],double wts[]);
int n_quad_rule(int deg, int ele_type);
void quad_rule_edge(int deg, double pts[], double wts[]);
int n_quad_rule_edge(int deg);
void timestamp();

#endif