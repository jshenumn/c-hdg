#include<iostream>
#include<vector>
#include<cmath>
#include<fstream>
#include<sstream>
#include"Basis_function.h"
#include"Physics.h"
#include"Mesh.h"
#include"Collocation.h"
#include"Quadrature.h"
#include"Local_solver.h"
#include"Global_solver.h"
#include<Eigen/Eigen>
#include<Eigen/Sparse>
#include<stdio.h>
#include<math.h>

using namespace std;
using namespace Eigen;


void reconstruction(VectorXd solUhat, VectorXd & solU, VectorXd & solQ, Element2D element_list[], Local_Mat Lmat_list[], int dim_m, int dim_w, int dim_v, int n_ele);

void validation(double &L2errQ, double &L2errU, double&L2errUstar, VectorXd &solU, VectorXd &solQ,VectorXd &solpU,std::function<double(double,double)> funcU, std::function<double * (double,double)> funcQ, int n_ele, Element2D element_list[], Point2D node_list[], double col_v_triangle[][2],double col_w_triangle[],Quad &QT, int dim_v, int dim_w, int dim_wstar);