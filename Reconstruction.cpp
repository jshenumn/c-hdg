#include<iostream>
#include<vector>
#include<cmath>
#include<fstream>
#include<sstream>
#include"Basis_function.h"
#include"Physics.h"
#include"Mesh.h"
#include"Collocation.h"
#include"Quadrature.h"
#include"Local_solver.h"
#include"Global_solver.h"
#include<Eigen/Eigen>
#include<Eigen/Sparse>
#include<stdio.h>
#include<math.h>

using namespace std;
using namespace Eigen;


void reconstruction(VectorXd solUhat, VectorXd & solU, VectorXd & solQ, Element2D element_list[], Local_Mat Lmat_list[], int dim_m, int dim_w, int dim_v, int n_ele){
    
    // loop over all element
    for (int i = 1; i <= n_ele; i++) {
     
        int * edgArr = new int[3];
        for (int ii = 0; ii < 3; ii++) {
            edgArr[ii] = 0;
        }
        
        edgArr = element_list[i].get_edge();
        
        VectorXd uhat(dim_m*3);
        uhat.setZero();
        
        for (int j = 0; j < 3; j++) {
             int target = edgArr[j];
             int auxGlob = (target-1)*dim_m;
            
             uhat.segment(j*dim_m,dim_m) = solUhat.segment(auxGlob,dim_m);
        }
        
        MatrixXd ML(dim_v,3*dim_m);  VectorXd rhsL(dim_v);
        ML.setZero(); rhsL.setZero();
        ML = Lmat_list[i-1].get_Aq();
        rhsL = Lmat_list[i-1].get_RHSq();
        
        MatrixXd ML2(dim_w,3*dim_m);  VectorXd rhsL2(dim_w);
        ML2.setZero(); rhsL2.setZero();
        ML2 = Lmat_list[i-1].get_Au();
        rhsL2 = Lmat_list[i-1].get_RHSu();

        
        // construct solQ
        int auxGlobv = (i-1)*dim_v;
        solQ.segment(auxGlobv,dim_v) = ML*uhat-rhsL;
        
        // construct solU
        int auxGlobw = (i-1)*dim_w;
        solU.segment(auxGlobw,dim_w) = ML2*uhat-rhsL2;
        
        
    }// element loop
    
    
    
    
    
}

void validation(double &L2errQ, double &L2errU, double&L2errUstar, VectorXd &solU, VectorXd &solQ,VectorXd &solpU,std::function<double(double,double)> funcU, std::function<double * (double,double)> funcQ, int n_ele, Element2D element_list[], Point2D node_list[], double col_v_triangle[][2],double col_w_triangle[],Quad &QT, int dim_v, int dim_w, int dim_wstar){
    
    double l2erQ  = 0.0;
    double l2erU  = 0.0;
    double l2erUstar  = 0.0;
    
    //obtain integration info
    int n_pt = QT.get_n_pt();
    double * wts = new double[n_pt];
    Point2D * pts = new Point2D[n_pt];
    
    wts = QT.get_wts();
    pts = QT.get_pts();
    
    for (int i = 1; i <= n_ele; ++i) {
    //compute L2 projection of funcQ
        VectorXd l2_proj_v(dim_v);
        l2_proj_v.setZero();
        L2projection_Vh(l2_proj_v, element_list[i], node_list, dim_v, col_v_triangle,QT, funcQ);
        
   //compute L2 projection of funcU
        VectorXd l2_proj_w(dim_w);
        l2_proj_w.setZero();
        L2projection_Wh(l2_proj_w, element_list[i], node_list, dim_w, col_w_triangle,QT, funcU);
    
   //compute L2 projection of funcU to postprocessing space
        double col_wstar_triangle[n_pt*dim_wstar];
        /*collocation recompute*/
        for (int j = 0; j < n_pt; ++j ) {
            for (int i = 0; i < dim_wstar; ++i) {
                col_wstar_triangle[i + j*dim_wstar] = basis_triangle(i+1,pts[j].xcord(),pts[j].ycord()); // store in a vector using vectorization convention.
            }
        }
        
        VectorXd l2_proj_wstar(dim_wstar);
        l2_proj_wstar.setZero();
        L2projection_Wh(l2_proj_wstar, element_list[i], node_list, dim_wstar, col_wstar_triangle, QT, funcU);
        
        
    //compute error integration
        double area = element_list[i].get_area();
        double intVal = 0.0;
        double intVal2 = 0.0;
        double intVal3 = 0.0;
        
        int auxGb1 = (i-1)*dim_v; //global index for q
        int auxGb2 = (i-1)*dim_w; //global index for u
        int auxGb3 = (i-1)*dim_wstar; // global index for ustar
        
        for (int j = 0; j < n_pt; j++) {// loop over quad points
            double app = 0.0;
            double app2 = 0.0;
            double app3 = 0.0;
            
            for (int k = 0; k < dim_v; k++) { // loop over all dim of v
                
                app = app + pow((solQ(auxGb1 + k) - l2_proj_v(k)),2.0) * (col_v_triangle[k + j*dim_v][0]*col_v_triangle[k + j*dim_v][0] + col_v_triangle[k + j*dim_v][1]*col_v_triangle[k + j*dim_v][1]);
            }
            
            for (int l = 0; l < dim_w; l++) { // loop over all dim of w
                
                app2 = app2 + pow((solU(auxGb2 + l) - l2_proj_w(l)),2.0) * pow(col_w_triangle[l + j*dim_w],2.0);
            }
            
            for (int m = 0; m < dim_wstar; m++) { // loop over all dim of w
                
                app3 = app3 + pow((solpU(auxGb3 + m) - l2_proj_wstar(m)),2.0) * pow(col_wstar_triangle[m + j*dim_wstar],2.0);
            }

            
            
            intVal = intVal + wts[j]*area*app;
            intVal2 = intVal2 + wts[j]*area*app2;
            intVal3 = intVal3 + wts[j]*area*app3;
            
        }
        
        l2erQ = l2erQ + intVal;
        l2erU = l2erU + intVal2;
        l2erUstar = l2erUstar + intVal3;
        
        
        
    }//element loop
    
    L2errQ = pow(l2erQ,0.5);
    L2errU = pow(l2erU,0.5);
    L2errUstar = pow(l2erUstar,0.5);
}