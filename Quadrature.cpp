/*
 Qudrature.cpp
 Author: Jiguang Shen (All funtions adapted from Jon Burkardt's code except LGWT)
 Date  : Apr 3rd, 2015
 -----------------------------------------------
 This file treats the general 1D & 2D quadrature rules for HDG methods
 */
#include<iostream>
#include<cmath>
#include<stdlib.h>
#include<vector>
#include"Quadrature.h"
#include"Mesh.h"

using namespace std;


// Test
/*
int main(){
    timestamp();
    int deg = 6;
    int n_pt = n_quad_rule_edge(deg);
    double *pts =  new double[n_pt];
    double *wts =  new double[n_pt];
    //quad_rule_edge(deg, pts, wts);
    LGWT(n_pt,0.0,1.0,pts,wts);
    
    cout << "pts" << endl;
    for (int i = 0; i < n_pt; ++i) {
        cout << pts[i] << endl;
    }
    cout << "weights" << endl;
    for (int i = 0; i < n_pt; ++i) {
        cout << wts[i] << endl;
    }

    delete [] pts;
    delete [] wts;
    
    
}
*/


/*
int main(){
    timestamp ( );
    int deg = 6;
    int n_pt = n_quad_rule_edge(deg);
    double *pts =  new double[n_pt];
    double *wts =  new double[n_pt];
    quad_rule_edge(deg, pts, wts);
    cout << "pts" << endl;
    for (int i = 0; i < n_pt; ++i) {
        cout << pts[i] << endl;
    }
    cout << "weights" << endl;
    for (int i = 0; i < n_pt; ++i) {
        cout << wts[i] << endl;
    }
    
    
    //cout << gm_rule_size(2,2) << endl;
}
*/
/*
int main(){
    timestamp();
    Quad QT;
    QT.set_ele_quad(4);
    int n_pt = QT.get_n_pt();
    double * wts = new double[n_pt];
    Point2D * pts = new Point2D[n_pt];
    
    wts = QT.get_wts();
    pts = QT.get_pts();
    for (int i = 0; i < n_pt; ++i) {
        cout << wts[i] << endl;
    }
    cout << "n_pt: \t" << n_pt << endl;
    for (int i = 0; i < n_pt; ++i) {
        cout << pts[i].xcord() << "\t" << pts[i].ycord() << endl;
    }
    cout << "n_pt: \t" << n_pt << endl;
}
*/


 

// implementation of class
Quad::Quad(){
    n_pt = 1;
    wts = new double[1];
    pts = new Point2D[1];
    wts[0] = 1.0;
    double coord[2] = {0.0,0.0};
    pts[0].set_coord(coord);
}


Quad::~Quad(){
    delete [] wts;
    delete [] pts;
}


void Quad::set_edge_quad(int deg){
    // determine the n_pt
    n_pt = n_quad_rule_edge(deg);
    
    // assign more memory if needed
    delete [] wts;
    delete [] pts;
    
    wts = new double[n_pt];
    double * qpts = new double[n_pt]; // proxy of points
    
    pts = new Point2D[n_pt];
    
    // determine the weights and pts from reference
    quad_rule_edge(deg,qpts,wts);
    
    //assign pts by using barycentric coordinates
    for (int i = 0; i < n_pt; ++i) {
        double coord[2];
        coord[0] = qpts[i];
        coord[1]= 1-qpts[i];
        pts[i].set_coord(coord); // set points for each Point object
    }
    delete [] qpts;
    
}


void Quad::set_ele_quad(int deg){
    // determine the n_pt
    n_pt = n_quad_rule(deg,2);
    
    // assign more memory if needed
    delete [] wts;
    delete [] pts;
    
    wts = new double[n_pt];
    double * qpts = new double[n_pt*2]; // proxy of points in 2D
    
    pts = new Point2D[n_pt];
    
    // determine the weights and pts from reference
    int rule = ceil(0.5*(deg-1));
    gm_rule_set(rule,2,n_pt,qpts,wts);
    
    //assign pts by using barycentric coordinates in triangle
    // only store lambda1 lambda2, and lambda3 = 1 - lambda1 - lambda2
    for (int i = 0; i < n_pt; ++i) {
        double coord[2];
        coord[0] = qpts[i*2];
        coord[1]=  qpts[i*2+1];
        pts[i].set_coord(coord); // set points for each Point object
    }
    delete [] qpts;

}







// implementation of funtions:


void LGWT(int n_pt, double a , double b, double pts[], double wts[]){
    
    double xu[n_pt];
    double y[n_pt];
    double y0[n_pt];
    double Lpp[n_pt];
    double ydiff[n_pt];
    
    double L[n_pt][n_pt+1];
    double Lp[n_pt][n_pt+1];
    
    
    double residual  = 1.0;
    
    //initialization array
    std::fill_n(y0,n_pt,2.0);
    std::fill_n(ydiff,n_pt,2.0);
    
    
    for (int ii = 0; ii < n_pt; ++ii) {
        xu[ii] = -1.0 + (2.0/(n_pt-1)) * ii;
        y[ii] = cos((2.0*ii + 1.0) * M_PI /(2 * (n_pt-1) + 2)) + (0.27/(n_pt)) * sin(M_PI*xu[ii] *(n_pt-1)/(n_pt+1));
    }
    
    
    
    while (residual > 1e-14) { // tolerance maybe changed if different n_pt are requested
        // initialization
        for (int i = 0; i < n_pt; ++i) {
            L[i][0] = 1.0; Lp[i][0] = 0.0;
            L[i][1] = y[i]; Lp[i][1] = 1.0;
        }
        
        for (int j = 1; j < n_pt; ++j) {
            for (int l = 0;l < n_pt; ++l) {
                L[l][j+1] =  ((2*(j + 1)-1)*y[l]*L[l][j] - j*L[l][j-1]) / (j+1);
            }
        }
        
        for (int k = 0; k < n_pt; ++k) {
            Lpp[k] = (n_pt+1)*(L[k][n_pt-1] - y[k]*L[k][n_pt]) / (1-y[k]*y[k]);
            y0[k] = y[k];
            y[k] = y0[k] - L[k][n_pt]/Lpp[k];
        }
        
        
        
        // compute residual
        for (int jj = 0; jj < n_pt; ++jj) {
            ydiff[jj] = std::abs(y[jj] - y0[jj]);
        }
        
        residual = *std::max_element(ydiff,ydiff+n_pt);
        
        
    }//end while
    
    
    for (int ii = 0; ii < n_pt; ++ii) {
        pts[ii] = (a * (1 -y[ii]) + b*(1 +y[ii])) / 2.0;
        wts[ii] = (b-a)/( (1-y[ii]*y[ii])*(Lpp[ii]*Lpp[ii])) * (n_pt+1)/n_pt * (n_pt+1)/n_pt ;
    }
    
}


int gm_rule_size(int rule, int dim_num){
    int arg =  dim_num + rule + 1;
    return i4_choose(arg,rule);
}


int i4_choose(int n , int k){
    int mn = min(k,n-k);

    if (mn < 0){
        return 0;
    }
    else{
        if (mn == 0) {
            return 1;
            }
        else{
            int mx = max(k,n-k);
            int val = mx + 1;
            for (int i = 2; i < mn+1; ++i) {
                val = (val*(mx + i))/i;
                }
            return val;
            }
        }
}


void comp_next ( int n, int k, int a[], bool *more, int *h, int *t ){
//****************************************************************************
//
//  Purpose:
//
//    COMP_NEXT computes the compositions of the integer N into K parts.
//
//  Discussion:
//
//    A composition of the integer N into K parts is an ordered sequence
//    of K nonnegative integers which sum to N.  The compositions (1,2,1)
//    and (1,1,2) are considered to be distinct.
//
//    The routine computes one composition on each call until there are no more.
//    For instance, one composition of 6 into 3 parts is
//    3+2+1, another would be 6+0+0.
//
//    On the first call to this routine, set MORE = FALSE.  The routine
//    will compute the first element in the sequence of compositions, and
//    return it, as well as setting MORE = TRUE.  If more compositions
//    are desired, call again, and again.  Each time, the routine will
//    return with a new composition.
//
//    However, when the LAST composition in the sequence is computed
//    and returned, the routine will reset MORE to FALSE, signaling that
//    the end of the sequence has been reached.
//
//    This routine originally used a STATIC statement to maintain the
//    variables H and T.  I have decided (based on an wasting an
//    entire morning trying to track down a problem) that it is safer
//    to pass these variables as arguments, even though the user should
//    never alter them.  This allows this routine to safely shuffle
//    between several ongoing calculations.
//
//  Example:
//
//    The 28 compositions of 6 into three parts are:
//
//      6 0 0,  5 1 0,  5 0 1,  4 2 0,  4 1 1,  4 0 2,
//      3 3 0,  3 2 1,  3 1 2,  3 0 3,  2 4 0,  2 3 1,
//      2 2 2,  2 1 3,  2 0 4,  1 5 0,  1 4 1,  1 3 2,
//      1 2 3,  1 1 4,  1 0 5,  0 6 0,  0 5 1,  0 4 2,
//      0 3 3,  0 2 4,  0 1 5,  0 0 6.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    09 July 2007
//
//  Author:
//
//    FORTRAN77 original version by Albert Nijenhuis, Herbert Wilf,
//    C++ translation by John Burkardt.
//
//  Reference:
//
//    Albert Nijenhuis, Herbert Wilf,
//    Combinatorial Algorithms for Computers and Calculators,
//    Second Edition,
//    Academic Press, 1978,
//    ISBN: 0-12-519260-6,
//    LC: QA164.N54.
//
//  Parameters:
//
//    Input, int N, the integer whose compositions are desired.
//
//    Input, int K, the number of parts in the composition.
//
//    Input/output, int A[K], the parts of the composition.
//
//    Input/output, bool *MORE.
//    Set MORE = FALSE on first call.  It will be reset to TRUE on return
//    with a new composition.  Each new call returns another composition until
//    MORE is set to FALSE when the last composition has been computed
//    and returned.
//
//    Input/output, int *H, *T, two internal parameters needed for the
//    computation.  The user should allocate space for these in the calling
//    program, include them in the calling sequence, but never alter them
//****************************************************************************
    
    int i;
    
    if ( ! ( *more ) )
    {
        *t = n;
        *h = 0;
        a[0] = n;
        for ( i = 1; i < k; i++ )
        {
            a[i] = 0;
        }
    }
    else
    {
        if ( 1 < *t )
        {
            *h = 0;
        }
        
        *h = *h + 1;
        *t = a[*h-1];
        a[*h-1] = 0;
        a[0] = *t - 1;
        a[*h] = a[*h] + 1;
        
    }
    
    *more = ( a[k-1] != n );
    
    return;
}


void gm_rule_set ( int rule, int dim_num, int point_num, double pts[],double wts[]){
//****************************************************************************
//
//  Purpose:
//
//    GM_RULE_SET sets a Grundmann-Moeller rule.
//
//  Discussion:
//
//    This is a revised version of the calculation which seeks to compute
//    the value of the weight in a cautious way that avoids intermediate
//    overflow.  Thanks to John Peterson for pointing out the problem on
//    26 June 2008.
//
//    This rule returns weights and abscissas of a Grundmann-Moeller
//    quadrature rule for the DIM_NUM-dimensional unit simplex.
//
//    The dimension POINT_NUM can be determined by calling GM_RULE_SIZE.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    26 June 2008
//
//  Author:
//
//    John Burkardt
//
//  Reference:
//
//    Axel Grundmann, Michael Moeller,
//    Invariant Integration Formulas for the N-Simplex
//    by Combinatorial Methods,
//    SIAM Journal on Numerical Analysis,
//    Volume 15, Number 2, April 1978, pages 282-290.
//
//  Parameters:
//
//    Input, int RULE, the index of the rule.
//    0 <= RULE.
//
//    Input, int DIM_NUM, the spatial dimension.
//    1 <= DIM_NUM.
//
//    Input, int POINT_NUM, the number of points in the rule.
//
//    Output, double W[POINT_NUM], the weights.
//
//    Output, double X[DIM_NUM*POINT_NUM], the abscissas.
//****************************************************************************

int *beta;
int beta_sum;
int d;
int dim;
int h;
int i;
int j;
int j_hi;
int k;
bool more;
int n;
double one_pm;
int s;
int t;
double weight;
        
s = rule;
d = 2 * s + 1;
k = 0;
n = dim_num;
one_pm = 1.0;
        
beta = new int[dim_num+1];
        
    for ( i = 0; i <= s; i++ )
        {
            weight = ( double ) one_pm;
            
            j_hi = max ( n, max ( d, d + n - i ) );
            
            for ( j = 1; j <= j_hi; j++ )
            {
                if ( j <= n )
                {
                    weight = weight * ( double ) ( j );
                }
                if ( j <= d )
                {
                    weight = weight * ( double ) ( d + n - 2 * i );
                }
                if ( j <= 2 * s )
                {
                    weight = weight / 2.0;
                }
                if ( j <= i )
                {
                    weight = weight / ( double ) ( j );
                }
                if ( j <= d + n - i )
                {
                    weight = weight / ( double ) ( j );
                }
            }
            
            one_pm = - one_pm;
            
            beta_sum = s - i;
            more = false;
            h = 0;
            t = 0;
            
            for ( ; ; )
            {
                comp_next ( beta_sum, dim_num + 1, beta, &more, &h, &t );
                
                wts[k] = weight;
                for ( dim = 0; dim < dim_num; dim++ )
                {
                    pts[dim+k*dim_num] = ( double ) ( 2 * beta[dim+1] + 1 )
                    / ( double ) ( d + n - 2 * i );
                }
                k = k + 1;
                
                if ( !more )
                {
                    break;
                }
            }
        }
        
        delete [] beta;
        
        return;
}


int n_quad_rule(int deg, int ele_type){
    // ele_type = 1: edge
    // ele_type = 2: triangle
    int val=0;
    
    if(ele_type == 2){
        int rule = ceil(0.5*(deg - 1));
        val =  gm_rule_size(rule, 2);
        }
    else{
        if(ele_type == 1){
            int rule = ceil(0.5*(deg - 1));
            val =  gm_rule_size(rule, 1);
            }
        else{
        
        }
            //Modification to use the gm routine
            //intval = ceil(0.5 *(deg + 1)); Original Code
        }
    return val;
}


int n_quad_rule_edge(int deg){
    return ceil(0.5*(deg+1));
}


void quad_rule_edge(int deg, double pts[], double wts[]){
    int n_pt = n_quad_rule_edge(deg);
    
    if (deg <= 1) {
        wts[0] = 1.0;
        pts[0] = 0.5;
    }
    else{
        LGWT(n_pt,0.0,1.0,pts,wts);
    }
    
}


void timestamp(){
//****************************************************************************
//
//  Purpose:
//
//    TIMESTAMP prints the current YMDHMS date as a time stamp.
//
//  Example:
//
//    May 31 2001 09:45:54 AM
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    24 September 2003
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    None
//
{
# define TIME_SIZE 40
        
        static char time_buffer[TIME_SIZE];
        const struct tm *tm;
        size_t len;
        time_t now;
        
        now = time ( NULL );
        tm = localtime ( &now );
        
        len = strftime ( time_buffer, TIME_SIZE, "%d %B %Y %I:%M:%S %p", tm );
        
        cout << time_buffer << "\n";
        
        return;
# undef TIME_SIZE
    }
}









