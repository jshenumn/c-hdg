#include "Mesh.h"
#include "Quadrature.h"
#include "Physics.h"
#include <Eigen/Eigen>// use eigen package to solve matrix inversion

using namespace Eigen;
using namespace std;

// function prototypes
void collocation_edge(int dim_m, int dim_w, int dim_v, double col_m[], double col_w[][3], double col_v[][6], Quad &QE);

void collocation_triangle(int dim_w, int dim_v, double col_w_triangle[], double col_jv_triangle[][4], double col_v_triangle[][2], Quad &QT);


void L2projection_Mh(VectorXd &l2_proj_m,Edge2D &edge, Point2D * node_list, int dim_m, double col_m_edge[], Quad & QE, std::function<double(double,double,int)> func);


void L2projection_Wh(VectorXd &l2_proj_w, Element2D &ele, Point2D node_list[], int dim_w, double col_w_triangle[], Quad &QT, std::function<double(double,double)> func);


void L2projection_Vh(VectorXd &l2_proj_v, Element2D &ele, Point2D node_list[], int dim_v, double col_v_triangle[][2], Quad &QT, std::function<double * (double,double)> func);


void div_R2(double divEle[], Element2D &ele, Edge2D edge_list[], int dim_v, int n_pt,double col_jv_triangle[][4]);
