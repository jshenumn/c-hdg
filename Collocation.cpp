/*
Collocation.cpp
Author: Jiguang Shen
Date  : Apr 3rd, 2015
-----------------------------------------------------------------------
This files computes the preliminaries for integration on reference element/edge
*/

#include <iostream>
#include <cmath>
#include "Collocation.h"
#include "Quadrature.h"
#include "Mesh.h"
#include "Physics.h"
#include "Basis_function.h"
#include <Eigen/Eigen>// use eigen package to solve matrix inversion

using namespace Eigen;
using namespace std;

void collocation_edge(int dim_m, int dim_w, int dim_v, double col_m_edge[], double col_w_edge[][3], double col_v_edge[][6], Quad &QE)
{
 
    int n_pt = QE.get_n_pt();
    Point2D * pts = new Point2D[n_pt];
    pts = QE.get_pts(); // proxy in this function

    
    // get collocation for M_h space
    for (int j = 0; j < n_pt; ++j ) {
        for (int i = 0; i < dim_m; ++i) {
            col_m_edge[i + j*dim_m] = basis_edge(i+1,pts[j].xcord()); // store in a vector using vectorization convention.
        }
    }
    
    // get collocation for W_h space
    for (int j = 0; j < n_pt; ++j) {
        for (int i = 0; i < dim_w; ++i) {
            col_w_edge[i + j*dim_w][0] = basis_triangle(i+1,0.0,pts[j].ycord());
            col_w_edge[i + j*dim_w][1] = basis_triangle(i+1,pts[j].xcord(),0.0);
            col_w_edge[i + j*dim_w][2] = basis_triangle(i+1,pts[j].ycord(),pts[j].xcord());
        }
    }
   
    
    // get collocation for V_h space
    for (int j = 0; j < n_pt; ++j) {
        for (int i = 0; i < dim_v; ++i) {
            
             double * coord1 = new  double[2];
             double * coord2 = new  double[2];
             double * coord3 = new  double[2];
            
            coord1 = basis_v(i+1,0.0,pts[j].ycord());
            coord2 = basis_v(i+1,pts[j].xcord(),0.0);
            coord3 = basis_v(i+1,pts[j].ycord(),pts[j].xcord());
            
            // set coord
            col_v_edge[i + j*dim_v][0] = coord1[0];
            col_v_edge[i + j*dim_v][1] = coord1[1];
            
            col_v_edge[i + j*dim_v][2] = coord2[0];
            col_v_edge[i + j*dim_v][3] = coord2[1];
            
            col_v_edge[i + j*dim_v][4] = coord3[0];
            col_v_edge[i + j*dim_v][5] = coord3[1];
            
            delete [] coord1;
            delete [] coord2;
            delete [] coord3;
            
        }
    }
}


void collocation_triangle(int dim_w, int dim_v,  double col_w_triangle[],  double col_jv_triangle[][4],  double col_v_triangle[][2], Quad &QT){
    
    int n_pt = QT.get_n_pt();
    Point2D * pts = new Point2D[n_pt];
    pts = QT.get_pts(); // proxy in this function
    
    // get collocation for W_h
    for (int j = 0; j < n_pt; ++j ) {
        for (int i = 0; i < dim_w; ++i) {
            col_w_triangle[i + j*dim_w] = basis_triangle(i+1,pts[j].xcord(),pts[j].ycord()); // store in a vector using vectorization convention.
        }
    }
    
    // get collocation for V_h space
    for (int j = 0; j < n_pt; ++j) {
        for (int i = 0; i < dim_v; ++i) {
            
             double * coord1 = new  double[2]; // take points from basis v
             double * coord2 = new  double[4]; // take points from basis jv
            
            coord1 = basis_v(i+1,pts[j].xcord(),pts[j].ycord());
            coord2 = jac_v(i+1,pts[j].xcord(),pts[j].ycord());
            
            // set coord
            col_v_triangle[i + j*dim_v][0] = coord1[0];
            col_v_triangle[i + j*dim_v][1] = coord1[1];
            
            col_jv_triangle[i + j*dim_v][0] = coord2[0];
            col_jv_triangle[i + j*dim_v][1] = coord2[1];
            
            col_jv_triangle[i + j*dim_v][2] = coord2[2];
            col_jv_triangle[i + j*dim_v][3] = coord2[3];
            
            delete [] coord1;
            delete [] coord2;

            
        }
    }
    
}


void L2projection_Mh(VectorXd &l2_proj_m, Edge2D &edge, Point2D node_list[], int dim_m,  double col_m_edge[], Quad &QE, std::function< double( double, double,int)> func){
    // intialize
    MatrixXd MTX(dim_m, dim_m);
    VectorXd RHS(dim_m);
    
    // get info from input variable:
    int n_pt = QE.get_n_pt();
    int marker = edge.get_type();
    int * vert =  new int[2]; // get vert global numbering
    vert = edge.get_vert();
    
    // get quad weights and pts
     double * wts = new  double[n_pt];
    Point2D * pts = new Point2D[n_pt];
    
    wts = QE.get_wts();
    pts = QE.get_pts();
    
    // construct mass matrix
    for (int i = 0; i < dim_m; ++i) {
        for (int j = 0; j < dim_m; ++j) {
            double val = 0.0;
            for (int k = 0; k < n_pt; ++k) {
                val = val + wts[k] * col_m_edge[i + k*dim_m]* col_m_edge[j + k*dim_m];
            }
            MTX(i,j) = val;
            MTX(j,i) = MTX(i,j); // by symmetry
        }
    }
    
    
    // construct RHS vector
    
    for (int i = 0; i < dim_m; ++i) {
         double val = 0.0;
        for (int k = 0; k < n_pt; ++k) {
             double l1 =  pts[k].xcord();
            
             double vX = node_list[vert[0]].xcord() + l1*(node_list[vert[1]].xcord() - node_list[vert[0]].xcord());
            
             double vY = node_list[vert[0]].ycord() + l1*(node_list[vert[1]].ycord() - node_list[vert[0]].ycord());
            
            val = val + wts[k] * col_m_edge[i + k*dim_m]*func(vX,vY,marker);
            
        }
        RHS(i) = val;
    }
    
    
    // solve MTX x = RHS
    
    l2_proj_m = MTX.partialPivLu().solve(RHS); // solve with partial pivot LU
    
    
}


void L2projection_Wh(VectorXd &l2_proj_w, Element2D &ele, Point2D node_list[], int dim_w,  double col_w_triangle[], Quad &QT, std::function< double( double, double)> func){
     // initialize local matrix
        MatrixXd MTX(dim_w, dim_w);
        VectorXd RHS(dim_w);
    
     // get integration info from quadrature on triangle
        int n_pt = QT.get_n_pt();
        double * wts = new  double[n_pt];
        Point2D * pts = new Point2D[n_pt];
    
        wts = QT.get_wts();
        pts = QT.get_pts();
    
    // get info from element
        int * cnt =  new int[3]; // get vert global numbering
        cnt = ele.get_cnt();
    
     // construct mass matrix
    for (int i = 0; i < dim_w; ++i) {
        for (int j = 0; j < dim_w; ++j) {
            double val = 0.0;
            for (int k = 0;  k < n_pt; ++k) {
                val = val + wts[k] * col_w_triangle[i + k*dim_w] * col_w_triangle[j + k*dim_w];
            }
            MTX(i,j) = val;
            MTX(j,i) = MTX(i,j);
        }
    }
    
    //  construct right hand side
    for (int i = 0; i < dim_w; ++i) {
        double val = 0.0;
        for (int k = 0; k < n_pt; ++k) {
             double l1 = pts[k].xcord();
             double l2 = pts[k].ycord();
             double X = l1*node_list[cnt[0]].xcord()  +  l2*node_list[cnt[1]].xcord() + (1-l1-l2)*node_list[cnt[2]].xcord();
             double Y = l1*node_list[cnt[0]].ycord()  +  l2*node_list[cnt[1]].ycord() + (1-l1-l2)*node_list[cnt[2]].ycord();
            val = val + wts[k]*col_w_triangle[i + k*dim_w]*func(X,Y);
        }
        RHS(i) = val;
    }
 

    // solve MTX\RHS
    
    l2_proj_w = MTX.partialPivLu().solve(RHS); // solve with partial pivot LU
    
}


void L2projection_Vh(VectorXd &l2_proj_v, Element2D &ele, Point2D node_list[], int dim_v,  double col_v_triangle[][2], Quad &QT, std::function< double * ( double, double)> func){
    // initialize local matrix
    MatrixXd MTX(dim_v, dim_v);
    VectorXd RHS(dim_v);
    
    // get integration info from quadrature on triangle
    int n_pt = QT.get_n_pt();
    double * wts = new double[n_pt];
    Point2D * pts = new Point2D[n_pt];
    
    wts = QT.get_wts();
    pts = QT.get_pts();
    
    // get info from element
    int * cnt =  new int[3]; // get vert global numbering
    cnt = ele.get_cnt();
    
    // construct mass matrix
    for (int i = 0; i < dim_v; ++i) {
        for (int j = 0; j < dim_v; ++j) {
            double val = 0.0;
            for (int k = 0;  k < n_pt; ++k) {
                val = val + wts[k] * (col_v_triangle[i + k*dim_v][0] * col_v_triangle[j + k*dim_v][0] + col_v_triangle[i + k*dim_v][1] * col_v_triangle[j + k*dim_v][1]);
            }
            MTX(i,j) = val;
            MTX(j,i) = MTX(i,j);
        }
    }
    
    //  construct right hand side
    for (int i = 0; i < dim_v; ++i) {
        double val = 0.0;
        for (int k = 0; k < n_pt; ++k) {
            double l1 = pts[k].xcord();
            double l2 = pts[k].ycord();
            double X = l1*node_list[cnt[0]].xcord()  +  l2*node_list[cnt[1]].xcord() + (1-l1-l2)*node_list[cnt[2]].xcord();
            double Y = l1*node_list[cnt[0]].ycord()  +  l2*node_list[cnt[1]].ycord() + (1-l1-l2)*node_list[cnt[2]].ycord();
            double * aux = new double[2];
            aux = func(X,Y);
            //cout << aux[0] << "\t" << aux[1] << endl;
            val = val + wts[k]*(col_v_triangle[i + k*dim_v][0]*aux[0] + col_v_triangle[i + k*dim_v][1]*aux[1]);
            delete [] aux;
        }
        RHS(i) = val;
    }

    // solve MTX\RHS
    
    l2_proj_v = MTX.partialPivLu().solve(RHS); // solve with partial pivot LU
    
}


void div_R2(double divEle[], Element2D &ele, Edge2D edge_list[], int dim_v, int n_pt,double col_jv_triangle[][4]){
    
    
    // get geom info from element
    double area  = ele.get_area();
    double * nrm = new double[6];
    nrm  = ele.get_normal(); // get all unit outer normal
    int * edges = new int[3];
    edges = ele.get_edge(); // get all edge
    
    
    double l1 = edge_list[edges[0]].get_length();
    double l2 = edge_list[edges[1]].get_length();
    
    // define local vector and matrix to compute gradient
    double grad_l1[2];
    double grad_l2[2];
    grad_l1[0] = (-1.0/(2.0*area))*l1*nrm[0]; grad_l1[1] = (-1.0/(2.0*area))*l1*nrm[1];
    grad_l2[0] = (-1.0/(2.0*area))*l2*nrm[2]; grad_l2[1] = (-1.0/(2.0*area))*l2*nrm[3];
    
    // compute divergence
    
    for (int i = 0; i < dim_v; ++i) {
        for (int j = 0; j < n_pt; ++j) {
            divEle[i + j*dim_v] = grad_l1[0]*col_jv_triangle[i + j*dim_v][0] + grad_l1[1]*col_jv_triangle[i + j*dim_v][2] +
            grad_l2[0]*col_jv_triangle[i + j*dim_v][1] + grad_l2[1]*col_jv_triangle[i + j*dim_v][3];
                    }
    }
    
    
}































