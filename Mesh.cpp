/*
Mesh.cpp
Author: Jiguang Shen
Date  : Apr 1st, 2015
---------------------------------------------------------
Implementations of following classes
 1.Element2D
 2.Edge2D
 3.Point2D
*/

#include<iostream>
#include<cmath>
#include<algorithm>
#include<vector>
#include"Mesh.h"


using namespace std;


/*
Implementation of Element2D class
*/
Element2D::Element2D(){
    
    //initialization
    area = 0.0;
    nrm[0] = 0.0; nrm[1] = 0.0;
    for (int i = 0; i < 3; ++i) {
        connectivity[i] = 0;
        tau[i] = 1.0;
        edg_gn[i] = 1;
        unormal[2*i] = 0.0; unormal[2*i+1] = 0.0;
        orientation[i] = 1;
    }
    
        orientation[0] = 1;
    
        orientation[1] = 1;
   
        orientation[2] = 1;
}

Element2D::Element2D(int (&cnt)[3]){
    
    //initialization
    area = 0.0;
    nrm[0] = 0.0; nrm[1] = 0.0;
    for (int i = 0; i < 3; ++i) {
        connectivity[i] = cnt[i];
        tau[i] = 1.0;
        edg_gn[i] = 1;
        unormal[2*i] = 0.0; unormal[2*i+1] = 0.0;
        orientation[i] = 1;
    }
    
    // ----- adjust edge orientation -----
    // edge #1
    if (cnt[1] > cnt[2]) {
        orientation[0] = -1;
    }
    // edge #2
    if (cnt[2] > cnt[0]) {
        orientation[1] = -1;
    }
    // edge #3
    if (cnt[0] > cnt[1]) {
        orientation[2] = -1;
    }
}

// functions
int Element2D::check_edge_in_element(int v1, int v2){
    int aux = -1;
    for (int i = 0; i < 3; ++i) {
        if (connectivity[i] == v1) { //if in
            // if vert #1
            if (i == 0 && connectivity[1] == v2) {
                aux = 2; // edge #3 in element
            }
            if (i == 0 && connectivity[2] == v2) {
                aux = 1; // edge #2 in element
            }
            
            // if vert #2
            if (i == 1 && connectivity[0] == v2) {
                aux = 2; // edge #3 in element
            }
            if (i == 1 && connectivity[2] == v2) {
                aux = 0; // edge #1 in element
            }
            
            // if vert #3
            if (i == 2 && connectivity[0] == v2) {
                aux = 1; // edge #2 in element
            }
            if (i == 2 && connectivity[1] == v2) {
                aux = 0; // edge #1 in element
            }
        }
    }
    
    return aux; // if not -1 means not in this element
    
}

void Element2D::compute_area(double (&vert)[6]){
    double dx, dy;
    dy = (vert[2]-vert[0])*(vert[5]-vert[1]);
    dx = (vert[3]-vert[1])*(vert[4]-vert[0]);
    area = 0.5*(dy-dx);
}

void Element2D::compute_normal(double (&vert)[6]){
    
    double norm;
    //----- first edge ------
    unormal[0] = -(vert[3]-vert[5]);  unormal[1] = (vert[2]-vert[4]);
    norm = sqrt(pow(unormal[0],2.0) + pow(unormal[1],2.0));
    unormal[0] = 1/norm * unormal[0];  unormal[1] = 1/norm * unormal[1];
    
    //----- second edg ------
    unormal[2] = -(vert[5]-vert[1]);  unormal[3] = (vert[4]-vert[0]);
    norm = sqrt(pow(unormal[2],2.0) + pow(unormal[3],2.0));
    unormal[2] = 1/norm * unormal[2];  unormal[3] = 1/norm * unormal[3];

    //----- third edg ------
    unormal[4] = -(vert[1]-vert[3]);  unormal[5] = (vert[0]-vert[2]);
    norm = sqrt(pow(unormal[4],2.0) + pow(unormal[5],2.0));
    unormal[4] = 1/norm * unormal[4];  unormal[5] = 1/norm * unormal[5];

}


void Element2D::set_connectivity(int (&cnt)[3]){
    for (int i = 0; i < 3; ++i){
        connectivity[i] = cnt[i];
        orientation[i] = 1;
    }
    // ----- adjust edge orientation -----
    
    if (cnt[1] > cnt[2]) {
        orientation[0] = -1;
    }
    // edge #2
    if (cnt[2] > cnt[0]) {
        orientation[1] = -1;
    }
    // edge #3
    if (cnt[0] > cnt[1]) {
        orientation[2] = -1;
    }
        
}


void Element2D::set_edg_gn(int edge_gn, int edge_ln){
    
     edg_gn[edge_ln] = edge_gn;
    
}


/*
 Implementation of Point2D class
 */
Point2D::Point2D(){
    x = 0.0;
    y = 0.0;
}

Point2D::Point2D(double (&coord)[2]){
    x = coord[0];
    y = coord[1];
}


void Point2D::set_coord(double (&coord)[2]){
    x = coord[0];
    y = coord[1];
}



/*
 Implementation of Edge2D class
 */
Edge2D::Edge2D(){
    //initialization
    vert[0] = 0; vert[1] = 0;
    length = 0.0;
}


Edge2D::Edge2D(int (&vt)[2]){
    //initialization
    vert[0] = vt[0]; vert[1] = vt[1];
    length = 0.0;
}

void Edge2D::set_vert(int (&vt)[2]){
     vert[0] = vt[0]; vert[1] = vt[1];
}


void Edge2D::set_length(Point2D &a, Point2D &b){
    length = sqrt(pow((a.xcord() - b.xcord()),2.0) + pow((a.ycord() - b.ycord()),2.0));
}

void Edge2D::set_type(int etype){
    edge_type = etype;
}



// read mesh function

vector<Row> mesh_read(string path){
    // Load table from file
    vector<Row> table;
    ifstream file(path);
    while (file){
        string line;
        getline(file, line);
        istringstream is(line);
        Row row;
        
        while (is){
            double data;
            is >> data;
            row.push_back(data);
            
            // if no numbers left move to next column
            if(!isdigit(is.peek())) {
                is.ignore();
                continue;
            }
        }
        table.push_back(row);
        
        // if no numbers left move to next row
        if(!isdigit(file.peek())) {
            file.ignore();
            continue;
        }
    }
    return table;
}

void make_mesh(int &n_ele, int &n_edge, Element2D * element_list, Edge2D * edge_list, Point2D * node_list, vector<int> &DBCarray, vector<Row> element_table, vector<Row> edge_table, vector<Row> node_table){
    
    
    /*
     Fold edge list into an struct array
     v2 is the vertices numbering from 1 to n_node
     gindx the corresponding global edge numbering
     */
    
    struct compressedlist{
        int gindx[3];
    } compr_edglist [node_table.size()];
    int p = 1; // v1 counter
    int q = 0; // v2 counter
    
    
    
    
    for (int r = 1; r < node_table.size() ; ++r) {
        // each row read vertices pari into an array
        double coord[2];
        
        for (int i = 1; i < 3; ++i){
            coord[i-1] = node_table[r][i];
        }
        
        node_list[r].set_coord(coord); // initialize element
        
        // initialize compr_edgelist
        compr_edglist[r].gindx[0] = 0;
        compr_edglist[r].gindx[1] = 0;
        compr_edglist[r].gindx[2] = 0;
        
    }
    
    
    for (int r = 1; r < edge_table.size() ; ++r) {
        int dinx = 0;
        // each row read vertices pari into an array
        int vt[2];
        int type = 0;
        
        for (int i = 1; i < 3; ++i){
            vt[i-1] = edge_table[r][i];
        }
        type = edge_table[r][3];
        if (type != 0) {
            DBCarray.push_back(r);
        }
        edge_list[r].set_type(type);
        edge_list[r].set_vert(vt); // initialize vert
        edge_list[r].set_length(node_list[vt[0]],node_list[vt[1]]); // set_length
        
        // compress list:
        if (edge_table[r][1] == p){ // if v1 is p
            compr_edglist[p].gindx[q] = r;
            q++;
        }
        else // if v1 is not p, must be p+1
        {
            q = 0; // reset v2 counter
            compr_edglist[p+1].gindx[q] = r;
            q++;
            p++;
        }
        
        
    }
    
    
    
    
    for (int r = 1; r < element_table.size() ; ++r) {
        // each row read connectivity into an array
        int cnt[3];
        
        for (int i = 1; i < 4; ++i){
            cnt[i-1] = element_table[r][i];
        }
        
        element_list[r].set_connectivity(cnt); // initialize cnt
        
        // compute area, normal requires vert_coord array
        double vert_coord[6];
        
        for (int j = 0; j<3; ++j) {
            vert_coord[2*j] = node_table[cnt[j]][1];
            vert_coord[2*j+1] = node_table[cnt[j]][2];
        }
        
        element_list[r].compute_area(vert_coord); // compute area
        element_list[r].compute_normal(vert_coord); // compute normal
        
        // set edge global number requires edge local number, this is done by
        // inspect through edge_table and find corresponding edge local position
        

        
        // since we have compr_edgelist, from connectivity list
        for (int id = 0; id < 3; id++) {
            for (int mm = 0; mm < 3; ++mm) {
                int gid = compr_edglist[cnt[id]].gindx[mm]; //get edge global number
            
                if (gid !=0) { //if not empty
                        int varry[2];
                        for (int ii = 1; ii < 3; ++ii){
                                    varry[ii-1] = edge_table[gid][ii];
                            }
                int edg_ln = element_list[r].check_edge_in_element(varry[0], varry[1]);
                    if (edg_ln != -1){ // if kth edge in r-th element, set edge info
                    element_list[r].set_edg_gn(gid,edg_ln);
                    }
                }
            
            }
            
        }// i loop
        
        
        
        
        
        
        
//        /*
//         optimize search, since edge_list is sorted from low to high.
//         Normal vertices show up three times in the first column except boundary edges
//         This narrows down the search region.
//        */
//        int counter = 0;
//        int smin = min(min(cnt[0],cnt[1]),cnt[2]);
//        int smax = max(max(cnt[0],cnt[1]),cnt[2]);
//        
//        for (int k = (smin-1)*2+1; k < smax*3; ++k) { //loop over all edges
//            if (counter == 3) { // cannot have more than 3 matches
//                break;
//            }
//            
//            int varry[2];
//            // gather vertices index
//            for (int ii = 1; ii < 3; ++ii){
//                varry[ii-1] = edge_table[k][ii];
//            }
//            
//            int edg_ln = element_list[r].check_edge_in_element(varry[0], varry[1]);
//            
//            if (edg_ln != -1){ // if kth edge in r-th element, set edge info
//                element_list[r].set_edg_gn(k,edg_ln);
//                counter++;
//            }
//            
//            if (k == edge_table.size()-1) { // cannot exceed table
//                break;
//            }
//            
//        } // edge loop
        
        
        
        
        
    }// element loop
    

    
    
    //
    
    n_ele = element_table.size()-1;
    n_edge = edge_table.size()-1;
}


