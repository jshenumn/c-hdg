#include<iostream>
#include<vector>
#include<cmath>
#include<fstream>
#include<sstream>
#include"Basis_function.h"
#include"Physics.h"
#include"Mesh.h"
#include"Collocation.h"
#include"Quadrature.h"
#include"Local_solver.h"
#include"Global_solver.h"
#include<Eigen/Eigen>
#include<Eigen/Sparse>
#include<time.h>
#include<stdio.h>
#include<math.h>


using namespace Eigen;
using namespace std;


void Global_Assemble(SpMat &A, VectorXd &rhs, Local_Mat Lmat_list[], int Mymap[][6], int n_ele, int n_edge, Element2D element_list[],int dim_m){

    // loop over all element to assign block values
    for (int i = 1; i <= n_ele; ++i) {
        
    int * edgarr = new int[3];
    edgarr = element_list[i].get_edge(); // get edge global numbering
   //cout << edgarr[0] << "\t" << edgarr[1] << "\t" << edgarr[2] <<endl;
        
        for (int ii = 0; ii < 3; ++ii) {// local edge loop
            
            int rindx = edgarr[ii]-1; // ! matrix indexing from 0 instead of 1
            //cout << rindx << endl;
            int gRow1 = dim_m*rindx; //again index from 0!
            //int gRow2 = gRow1+dim_m-1;
            
            
            int lRow1 = dim_m*ii;
            //int lRow2 = lRow1+dim_m-1;
            
            
            for (int jj = ii; jj < 3; jj++) {
                
                int cindx = edgarr[jj]-1;
                
                //cout << cindx << endl;
                
                int gCol1 = dim_m*cindx; //again index from 0!
                //int gCol2 = gCol1+dim_m-1;
                
                
                int lCol1 = dim_m*jj;
                //int lCol2 = lCol1+dim_m-1;
                

            // insert contribution to global matrix
            MatrixXd K(3*dim_m,3*dim_m);
            K = Lmat_list[i-1].get_KK();
            MatrixXd Kproxy(dim_m,dim_m);
            //cout << K << endl;
            Kproxy = K.block(lRow1,lCol1,dim_m,dim_m); // need this proxy since K is not sparse matrix
            
            // fill sparse block
                for (int r = 0; r < dim_m; r++) {
                    for (int c = 0; c < dim_m; c++) {
                        //cout << A.coeffRef(r + gRow1,c + gCol1) << endl;
                        A.coeffRef(r + gRow1,c + gCol1) -= Kproxy(r,c); // insertion is order 1
                    }
                }
              
            // insert block map
               insertmap(Mymap,rindx,cindx);
            
               
            // use symmetry to insert the other half
                if (cindx !=rindx) { // fill sparse block
                    Kproxy = K.block(lCol1,lRow1,dim_m,dim_m);
                    for (int r = 0; r < dim_m; r++) {
                        for (int c = 0; c < dim_m; c++) {
                            A.coeffRef(c + gCol1,r + gRow1) -= Kproxy(c,r); // insertion is order 1
                        }
                    }
                    insertmap(Mymap,cindx,rindx);
                }
                
            }//jj loop
         
            
        // process RHS
           VectorXd aux(dim_m);
           aux = Lmat_list[i-1].get_BB();
           
           rhs.segment(gRow1,dim_m) = rhs.segment(gRow1,dim_m) + aux.segment(lRow1,dim_m);
            
            
        }// ii loop
    
        
    }// element loop
    
    A.makeCompressed(); // compress storage
    
    }


void dirichletBC(SpMat &A, VectorXd &rhs, int Mymap[][6], vector<int> DBCarray, Edge2D edge_list[], Point2D node_list[], double col_m_edge[], int dim_m, function<double(double,double,int)> func, Quad &QE){
    
    for (int i = 0; i < DBCarray.size(); i++) {
        
        int edgindx = DBCarray[i];
        //cout << "edgindx \t" << edgindx << endl;
        
        // compute l2 projection
        
        VectorXd l2_proj_m(dim_m);
        l2_proj_m.setZero();
        
        L2projection_Mh(l2_proj_m, edge_list[edgindx], node_list, dim_m, col_m_edge, QE, boundary_g);
        
        
        // modify global matrix
        int glob1 = (edgindx-1)*dim_m;
        //int glob2 = glob1+dim_m-1;
        //cout << "glob1, glob2: \t";
        //cout << glob1 << "\t" << glob2 <<endl;
        
        // block position is given by Mymap
        int arry[5];
        for (int j = 1; j < 6; j++) {
            arry[j-1] = Mymap[edgindx-1][j];
            //cout << arry[j-1] << endl;
            
            if (arry[j-1] != -1) { //-1 means this block has been deleted
                
                int glob1Aux = (arry[j-1])*dim_m;
                //int glob2Aux = glob1Aux+dim_m-1;
                
                //cout << "glob1Aux, glob2Aux: \t";
                //cout << glob1Aux << "\t" << glob2Aux <<endl;
                
                if (arry[j-1] != (edgindx-1)) {
                    
                    VectorXd vec2(dim_m);
                    vec2 = A.block(glob1Aux, glob1, dim_m, dim_m) * l2_proj_m;
                    //cout << "HERE" << endl;
                    rhs.segment(glob1Aux, dim_m) = rhs.segment(glob1Aux, dim_m) - vec2;

                    //set blocks to be zero
                    for (int r = 0; r < dim_m; r++) {
                        for (int c = 0; c < dim_m; c++) {
                            //cout << A.coeffRef(r + gRow1,c + gCol1) << endl;
                            A.coeffRef(r + glob1,c + glob1Aux) = 0.0; // insertion is order 1
                            A.coeffRef(c + glob1Aux, r + glob1) = 0.0;
                        }
                    }
                    // modifiy Mymap
                    Mymap[edgindx-1][j] = -1; // indicate already deleted
                    for (int k = 1; k < 6; k++) { // inspect the block for deleted element
                        if (Mymap[arry[j-1]][k] == edgindx-1) {
                            Mymap[arry[j-1]][k] = -1;
                        }
                        
                    }
                    
                    
                }
                
                else // modify RHS
                {
                    //cout << l2_proj_m << endl;
                    //cout << "modifed place:" << glob1Aux << endl;
                    rhs.segment(glob1Aux,dim_m) = l2_proj_m;
                    // block is diagonal
                    for (int r = 0; r < dim_m; r++) {
                        for (int c = 0; c < dim_m; c++) {
                            //cout << A.coeffRef(r + gRow1,c + gCol1) << endl;
                            if (r == c) {
                                A.coeffRef(r + glob1,c + glob1) = 1.0;
                            }
                            else{
                                A.coeffRef(r + glob1,c + glob1) = 0.0;
                            }
                        }
                    }
                    
                }// end else
                
            }
            
            
            
            }//j loop
        
            
        }// i loop
    
    
}




void insertmap(int Mymap[][6], int indx, int value){
    
    //Mymap is a 2D array that each row stores 5 element
    //first element of each row is an indicator to show how many elements were inserted
    //initially there's 0 element.
    
    // if not inserted
    if (Mymap[indx][0] == 0){
        Mymap[indx][1] = value;
        Mymap[indx][0] = Mymap[indx][0] + 1 ; // indicate there's one element inserted
    }
    else
    {
        int n_insert = Mymap[indx][0]; // number of element inserted
        for (int i = 1; i <= n_insert; ++i) {
            if (Mymap[indx][i] == value){
                return; // if already inserted exit
            }
        }
        Mymap[indx][0] = Mymap[indx][0] + 1;
        Mymap[indx][n_insert+1] = value;
    }
    
}







