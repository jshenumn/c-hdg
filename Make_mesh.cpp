/*
 Make_mesh.cpp
 Author: Jiguang Shen
 Date  : Apr 1st, 2015
 -----------------------------------
 A simple Read mesh data from triangle mesh generator
 and process the mesh. For example, compute unit outer normal.
 
 -----------------------------------
 Read mesh data from triangle mesh generator
 it provides three files:
 .ele : element file with connectivity
 .edge: edge file with vertices
 .node: node file with coordinate
 
 */

#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <cmath>
#include "Mesh.h"
using namespace std;


int main()
{
    
    string path_ele = "./MESH/MESH.2.ele";
    string path_edge = "./MESH/MESH.2.edge";
    string path_node = "./MESH/MESH.2.node";
    
    
    // read element and initialize element objects
    vector<Row> element_table = mesh_read(path_ele);
    /*
    ------------- The format of .ele file -------------------
    # element id      # connectivity  1 by 3 vector  # element type 0 or 1
    ! first row contains data information.
    */
    
    // read edge and initialize edge objects
    vector<Row> edge_table = mesh_read(path_edge);
    /*
     ------------- The format of .edge file -------------------
     # edge id      # vert1   #vert2  # element type 0(boundary) or 1-4
     ! first row contains data information.
     */
    
    // read edge and initialize edge objects
    vector<Row> node_table = mesh_read(path_node);
    /*
     ------------- The format of .edge file -------------------
     # point id       x   y  # element type 0(boundary) or 1-4
     ! first row contains data information.
     */

    
    
    
    
    Element2D *element_list = new Element2D[element_table.size()-1];
    
    for (int r = 1; r < element_table.size() ; ++r) {
        // each row read connectivity into an array
        int cnt[3];
        
        for (int i = 1; i < 4; ++i){
            cnt[i-1] = element_table[r][i];
        }
        
        element_list[r].set_connectivity(cnt); // initialize cnt
        
        // compute area, normal requires vert_coord array
        double vert_coord[6];
        
        for (int j = 0; j<3; ++j) {
            vert_coord[2*j] = node_table[cnt[j]][1];
            vert_coord[2*j+1] = node_table[cnt[j]][2];
        }
        
        element_list[r].compute_area(vert_coord); // compute area
        element_list[r].compute_normal(vert_coord); // compute normal
        
        // set edge global number requires edge local number, this is done by
        // inspect through edge_table and find corresponding edge local position
        for (int k = 1; k < edge_table.size(); ++k) { //loop over all edges
            int varry[2];
            // gather vertices index
            for (int ii = 1; ii < 3; ++ii){
                varry[ii-1] = edge_table[k][ii];
            }
            
            int edg_ln = element_list[r].check_edge_in_element(varry[0], varry[1]);
            
            if (edg_ln != -1){ // if kth edge in r-th element, set edge info
                element_list[r].set_edg_gn(k,edg_ln);
            }
            
        } // edge loop
    }// element loop
    
    
    Edge2D *edge_list = new Edge2D[edge_table.size()-1];
    
    for (int r = 1; r < edge_table.size() ; ++r) {
        // each row read vertices pari into an array
        int vt[2];
        
        for (int i = 1; i < 3; ++i){
            vt[i-1] = edge_table[r][i];
        }
        
        edge_list[r].set_vert(vt); // initialize vert
        
        // test
        int * vert = new int[2];
        vert = edge_list[r].get_vert();
        cout << vert[0] << "\t" << vert[1] << endl;
        
    }
    
    
    Point2D * node_list = new Point2D[node_table.size()-1];
        
        for (int r = 1; r < node_table.size() ; ++r) {
            // each row read vertices pari into an array
            double coord[2];
            
            for (int i = 1; i < 3; ++i){
                coord[i-1] = node_table[r][i];
            }
            
            node_list[r].set_coord(coord); // initialize element
            
        }
    
    return 0;
}

