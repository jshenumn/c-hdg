//dimension construction
int number_basis_w(int deg);
int number_basis_v(int deg);
int number_basis_m(int deg);
int number_basis_RT(int deg);
int number_basis_g(int deg);


// simple factorial function
int factorial(int n);
// evaluating n-th legendre polynomial at x
double legendre_poly(double x, int n);
// evaluating l-th edge basis on reference edge at barycentric coordinate l
double basis_edge(int j, double l);


// evaluating n-th (a,b)-jacobi polynomial at x
double jacobi_poly(double x, int a, int b, int n);
// evaluating derivative n-th (a,b)-jacobi polynomial at x
double Djacobi_poly(double x, int a, int b, int n);
// evaluating j-th triangle basis at barycentric coordinate (l1, l2)
double basis_triangle(int j, double l1, double l2);
// evaluating x partial derivative of j-th triangle basis at barycentric coordinate (l1, l2)
double DL1_bases_triangle(int j, double l1, double l2);
// evaluating y partial derivative of j-th triangle basis at barycentric coordinate (l1, l2)
double DL2_bases_triangle(int j, double l1, double l2);


// construct basis function for vector basis V_h
double * basis_v(int j, double l1, double l2);
// construct gradient of basis function for scalar space W_h
double * grad_basis_w(int j, double l1, double l2);
// construct jacobian matrix for vector basis V_h
double * jac_v(int j, double l1, double l2);
// define scalar space
double basis_w(int j, double l1, double l2);
// define multiplier space
double basis_m(int j, double l);


// extra functions may be used for other PDEs (by default consider only Laplacian)

// construct basis function for 2 by 2 tensor basis G_h
double * basis_g(int j, double l1, double l2);
// construct basis function for vector Raviart Thomas basis RT_h
double * basis_RT(int j, double l1, double l2, int deg);
// construct jacobi matrix for tensor basis function
double * jac_g(int j,double l1,double l2);
