/*
  Mesh.h
  Author: Jiguang Shen
  Date  : Apr 1st, 2015
 
-----------------------------------------------
 This header file contains class definitions:
 1.Element2D
 2.Edge2D
 3.Point2D
*/

#ifndef MESH_H
#define MESH_H

#include<vector>
#include<cmath>
#include <fstream>
#include <iostream>
#include <sstream>
using namespace std;

typedef vector<double> Row;

class Element2D{
public:
        Element2D();
        Element2D(int (&cnt)[3]);
        //Element2D(const Element2D & ele){};
        //Element2D & operator = (const Element2D & ele);
        ~Element2D(){};
    
      // accessors
        double get_area(){return area;};
        int * get_cnt(){return connectivity;};
        int get_ori(int edg_ln){return orientation[edg_ln-1];};
        int get_edg_gn(int edg_ln){return edg_gn[edg_ln];};
        double * get_nrm(int edg_ln){
            nrm[0] = unormal[(edg_ln-1)*2]; nrm[1] = unormal[(edg_ln-1)*2 +1];
            return nrm;
        }
        double * get_normal(){return unormal;};
        int * get_edge(){return edg_gn;};
        double * get_tau(){return tau;};
    
    
    
    
        // functions
        int check_edge_in_element(int v1, int v2); // check the if an edge is in the element
    
    
        void compute_area(double (&vert)[6]);
        void compute_normal(double (&vert)[6]);
        void set_edg_gn(int edg_gn, int edge_ln);
        void set_connectivity(int (&cnt)[3]);
    
    
    
private:
        double area;
        int  connectivity[3];
        int  orientation[3];
        int  edg_gn[3];
        double  tau[3];
        double  unormal[6];
        double  nrm[2]; // a passer to hand in normal on each edge
    
};


class Point2D{
public:
        Point2D();
        Point2D(double (&coord)[2]);
       ~Point2D(){};
    
        //accessors
        void set_coord(double (&coord)[2]);
        double xcord(){return x;};
        double ycord(){return y;};
    
private:
        double x;
        double y;
    
};


class Edge2D{
public:
    Edge2D();
    Edge2D(int (&vert)[2]);
    //Edge2D(const Edge2D & edg){};
    //Edge2D & operator = (const Edge2D & edg);
    ~Edge2D(){};
    
    //accesors
    int * get_vert(){return vert;};
    double get_length(){return length;};
    int get_type(){return edge_type;};
    
    //functions
    void set_vert(int (&vert)[2]);
    void set_length(Point2D &a, Point2D &b);
    void set_type(int etype);
    
private:
    int vert[2];
    double length;
    double edge_type;
    
};

// reader function
vector<Row> mesh_read(string path);

void make_mesh(int &n_ele, int &n_edge, Element2D * element_list, Edge2D * edge_list, Point2D * node_list, vector<int> &DBCarray, vector<Row> element_table, vector<Row> edge_table, vector<Row> node_table);

#endif
