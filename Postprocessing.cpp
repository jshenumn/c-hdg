#include<iostream>
#include<vector>
#include<cmath>
#include<fstream>
#include<sstream>
#include"Basis_function.h"
#include"Physics.h"
#include"Mesh.h"
#include"Collocation.h"
#include"Quadrature.h"
#include"Local_solver.h"
#include"Global_solver.h"
#include"Reconstruction.h"
#include"Postprocessing.h"
#include<Eigen/Eigen>
#include<Eigen/Sparse>
#include<time.h>
#include<stdio.h>
#include<math.h>


using namespace std;
using namespace Eigen;

void postprocessing(VectorXd &solpU, VectorXd &solQ, double &avgU, Element2D& ele, Edge2D edge_list[], int dim_v, int dim_wstar,double col_wstar_triangle[],double col_v_triangle[][2],Quad &QT){
    
// create a new collocation matrix for wh star space
    int n_pt = QT.get_n_pt();
    double * wts = new  double[n_pt];
    Point2D * pts = new Point2D[n_pt];
    double area = ele.get_area();
    
    wts = QT.get_wts();
    pts = QT.get_pts();
    
// perform elementwise postprocessing
   
    /*construct mass matrices*/
    MatrixXd MTX(dim_wstar-1,dim_wstar-1);
    double gradEle[dim_wstar*n_pt][2];
    grad_R2(gradEle, ele, edge_list,dim_wstar, QT);
    for (int ii = 0; ii < dim_wstar-1; ii++) {
        for (int jj = 0; jj < dim_wstar-1; jj++){
            double sum = 0.0;
            for (int kk = 0; kk < n_pt; kk++){
             
                sum = sum + wts[kk]*(gradEle[ii+1 + kk*dim_wstar][0]*gradEle[jj+1 + kk*dim_wstar][0]+ gradEle[ii+1 + kk*dim_wstar][1]*gradEle[jj+1 + kk*dim_wstar][1]);
                
            }
            
            MTX(ii,jj) = area*sum;
        }
    }

    /*construct right hand side*/
    MatrixXd MTXR(dim_wstar-1,dim_v); MTXR.setZero();
    VectorXd RHS(dim_wstar-1);RHS.setZero();
    for (int ii = 0; ii < dim_wstar-1; ii++) {
        for (int jj = 0; jj < dim_v; jj++){
            double sum = 0.0;
            for (int kk = 0; kk < n_pt; kk++){
                
                sum = sum + wts[kk]*(col_v_triangle[jj + kk*dim_v][0]*gradEle[ii+1 + kk*dim_wstar][0]+ col_v_triangle[jj + kk*dim_v][1]*gradEle[ii+1 + kk*dim_wstar][1]);
            }
            
            MTXR(ii,jj) = area*sum;
        }
    }
    
    RHS = MTXR*solQ;
    

    /*construct output solpU*/
    
    // first component is the same as solU
    solpU(0) = avgU;
    VectorXd proxy(dim_wstar-1); proxy.setZero();
    RHS = proxy - RHS;
    
    // the remaining from matrix equations
    solpU.segment(1,dim_wstar-1) = MTX.partialPivLu().solve(RHS);
}



void grad_R2(double gradEle[][2], Element2D &ele, Edge2D edge_list[], int dim_w, Quad &QT){
    
    // get geom info from element
    int n_pt = QT.get_n_pt();
    double area  = ele.get_area();
    double * nrm = new double[6];
    nrm  = ele.get_normal(); // get all unit outer normal
    int * edges = new int[3];
    edges = ele.get_edge(); // get all edge
    Point2D * pts = new Point2D[n_pt];
    pts = QT.get_pts();
    
    
    
    double l1 = edge_list[edges[0]].get_length();
    double l2 = edge_list[edges[1]].get_length();
    
    // define local vector and matrix to compute gradient
    double grad_l1[2];
    double grad_l2[2];
    grad_l1[0] = (-1.0/(2.0*area))*l1*nrm[0]; grad_l1[1] = (-1.0/(2.0*area))*l1*nrm[1];
    grad_l2[0] = (-1.0/(2.0*area))*l2*nrm[2]; grad_l2[1] = (-1.0/(2.0*area))*l2*nrm[3];
    
    // compute divergence
    for (int i = 0; i < dim_w; ++i) {
        for (int j = 0; j < n_pt; ++j) {
            
            double * v = new double[2];
            v = grad_basis_w(i+1,pts[j].xcord(),pts[j].ycord());
            
            gradEle[i + j*dim_w][0] = grad_l1[0]*v[0] + grad_l2[0]*v[1];
            gradEle[i + j*dim_w][1] = grad_l1[1]*v[0] + grad_l2[1]*v[1];
            delete [] v;
        }
    }
}

