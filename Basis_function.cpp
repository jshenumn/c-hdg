/*
Basis_function.cpp
Author: Jiguang Shen
Date  : Apr 3rd, 2015
------------------------------------
This file defines functions to construct basis functions for HDG methods
*/
#include<iostream>
#include<cmath>
#include "Basis_function.h"
using namespace std;


/* test
int main(){
    cout.precision(20);
    
    double * vv = new double[4];
    
    vv = jac_v(3, 0.4, 0.6);
    
    cout << vv[0] << "\t" << vv[2] << endl;
    cout << vv[1] << "\t" << vv[3] << endl;
}
*/




//implementations of functions

double legendre_poly(double x, int n){
    double p0 = 1.0;
    double val;
    
    if(n == 0){ // L1(x) = 1
        return p0;
        }
    else{
        double p1 = x*p0;
        if(n == 1){
                val = p1;
        }// L2(x) = x
        else
        
        for (int ii = 2; ii < n+1; ++ii){
            double pn =(x*(2.0*ii-1.0)*p1-(ii-1)*p0)/(1.0*ii); //%Recurrence
            p0 = p1;
            p1 = pn;
            val = pn;
        }
        return val;
        } // end else
} // end function

double basis_edge(int j, double l){
    double val = legendre_poly(2*l - 1, j-1) *  sqrt(0.5*(2*j - 1)); // normalization constant
    return val;
}

double jacobi_poly(double x, int a, int b, int n){
    double p0 = 1;
    double p1 = 0.5*((a-b)+(a+b+2.0)*x);
    double val;
    
    if(n == 0){
        return p0;
    }
    
    else{
        if (n == 1){
            return p1;
        }
        else{
            for (int ii = 2; ii < n+1; ++ii){
            double c0=-2.0*(ii-1.+a)*(ii-1.0+b)*(2.0*(ii-1.)+a+b+2.0);
            double c2=2.0*(ii-1.+1.0)*(ii-1.0+a+b+1.0)*(2.0*(ii-1.)+a+b);
            double c11=(2.0*(ii-1.0)+a+b+1.0)*(a*a-b*b);
            double c12=(2.0*(ii-1.0)+a+b+2.0)*(2.0*(ii-1.0)+a+b+1.0)*(2.0*(ii-1.0)+a+b);
            double pn = ((c11+c12*x)*p1+c0*p0)/c2;
            p0 = p1;
            p1 = pn;
            val  = pn;
            }
            return val;
            }
        }
}

double Djacobi_poly(double x, int a, int b, int n){
    if(n != 0){
        return 0.5*(n+a+b+1.0)*jacobi_poly(x,a+1,b+1,n-1);
    }
    else{
        return 0.0;
        }
}

int factorial(int n){
    return (n == 1 || n == 0) ? 1 : factorial(n - 1) * n;
}

double basis_triangle(int j, double l1, double l2){
    /*
    Some reading is needed to understand this function:
    tensor product basis function  +  collapsed coordinate
     
    Reference: paper by Jie Shen et.al
    */
    double bb = 2.0*l2-1.0;
    double aa = 2.0*l1/(1.0-l2)-1.0;
    
    // Define the normalization constant
    // The ceil-based generalized Jacobi Polynomial
    double x=-1.5+0.5*sqrt(1.0+8.0*j);
    double theta_j = sqrt(8.0);
    double nm = ceil(x);
    double nm1 = (nm+1)*(nm+2);
    
    // The ceil-based expansion with degree n and m
    double n = nm1/2-j;
    double m = nm-n;
    
    
    double qr = 0.0;
    
    if (l2 == 1){      //if the evaluate at vertex 2 tensor product basis evaluate at a special point
        for (int ii = 0; ii < n+1; ++ii){
            qr = qr + pow((factorial(n)/(factorial(n-ii)*factorial(ii))),2.0)*pow(2*l1,n);
                                      }
                }
    else{           // Otherwise a normal case
        qr = jacobi_poly(aa,0,0,n)*pow(1.0-bb,n);
        }
    
    return jacobi_poly(bb,2*n+1,0,m)*qr*theta_j;

}

double DL1_bases_triangle(int j, double l1, double l2){
    
    // The collapsed coordinate of reference triangle
    double aa = 2.0*l1/(1.0-l2)-1.0;
    double bb = 2.0*l2-1.0;
    
    // normalization constant
    double x = -1.5+0.5*sqrt(1.+8.0*j);
    double theta_j = sqrt(8.0);
    
    
    // definte the degree of expansion for ceil-based Jacobi Polynomial
    double nm = ceil(x);
    double nm1 = (nm+1)*(nm+2);
    double n = nm1/2-j;
    double m = nm-n;
    
    return Djacobi_poly(aa,0,0,n)*jacobi_poly(bb,2*n+1,0,m)*pow(1.0-bb,n)*theta_j*2.0/(1.0-l2);

}

double DL2_bases_triangle(int j, double l1, double l2){
    // The collapsed coordinate of reference triangle
    double aa = 2.0*l1/(1.0-l2)-1.0;
    double bb = 2.0*l2-1.0;
    
    // normalization constant
    double x = -1.5+.5*sqrt(1.0+8.0*j);
    double theta_j=sqrt(8.0);
    
    // definte the degree of expansion for ceil-based Jacobi Polynomial
    double nm = ceil(x);
    double nm1 = (nm+1)*(nm+2);
    double n = nm1/2-j;
    double m = nm-n;
    
    // Take derivative in the argument l2
    double aux1 = jacobi_poly(bb,2*n+1,0,m)*pow(1.0-bb,n)*Djacobi_poly(aa,0,0,n)*2.0*l1*pow(1-l2,-2);
    double aux2 = Djacobi_poly(bb,2*n+1,0,m)*pow(1.0-bb,n)*jacobi_poly(aa,0,0,n)*2.0;
    double aux3 = jacobi_poly(bb,2*n+1,0,m)*jacobi_poly(aa,0,0,n)*n*pow(1.0-bb,n-1)*(-2.0);
    
    return (aux1+aux2+aux3)*theta_j;
    
}

double * basis_v(int j, double l1, double l2){
    
    double * vval = new double[2];
    
    if(j % 2 == 0){
        vval[0] = 0.0;
        vval[1] = basis_triangle(int(j/2),l1,l2);
    }
    
    // all odd number labeled basis has the form (phi_{(j+1)/2},0)'
    else{
        vval[0] = basis_triangle(int((j+1)/2),l1,l2);
        vval[1] = 0.0;
        }
    return vval;
    delete [] vval;
}

double * grad_basis_w(int j, double l1, double l2){
   
    double * vval = new double[2];
    
    vval[0] =  DL1_bases_triangle(j,l1,l2);
    vval[1] =  DL2_bases_triangle(j,l1,l2);
    
    return vval;
    delete [] vval;
}

double * jac_v(int j, double l1, double l2){
    
    double * jaco = new double[4]; // instead of 2 by 2 matrix store it in a vector use the convention of vectorization
    
    
    if(j % 2  == 1){ // When j is odd , the vector bais function is  (phi_{(j+1)/2},0)'

        
    jaco[0] = DL1_bases_triangle(int((j+1)/2),l1,l2);
    jaco[1] = DL2_bases_triangle(int((j+1)/2),l1,l2);
    jaco[2] = 0.0;
    jaco[3] = 0.0;
    
    }
    else{ // When j is even , the vector bais function is  (0,phi_{j/2})'
        
    jaco[0] = 0.0;
    jaco[1] = 0.0;
    jaco[2] = DL1_bases_triangle(int(j/2),l1,l2);
    jaco[3] = DL2_bases_triangle(int(j/2),l1,l2);
    
    }
    
    return jaco;
    delete [] jaco;

}

double basis_w(int j, double l1, double l2){
    return basis_triangle(j,l1,l2);
}

double basis_m(int j, double l){
    return basis_edge(j,l);
}

int number_basis_w(int deg){
    return int(0.5*(deg+1)*(deg+2));
}

int number_basis_v(int deg){
    return int((deg+1)*(deg+2));
}

int number_basis_m(int deg){
    return deg + 1;
}

int number_basis_RT(int deg){
    return int((deg+1)*(deg+3));
}

int number_basis_g(int deg){
    return int(2*(deg+1)*(deg+2));
}


// extra basis functon implementataions
double * basis_g(int j, double l1, double l2){
    
double * vval =  new double[4];
    
switch (j%4){
    
    case 0:
            vval[0] = 0.0;
            vval[1] = 0.0;
            vval[2] = 0.0;
            vval[3] = basis_triangle(int(j/4),l1,l2);
        break;
    case 1:
            vval[0] = basis_triangle(int((j+3)/4),l1,l2);
            vval[1] = 0.0;
            vval[2] = 0.0;
            vval[3] = 0.0;
        break;
    case 2:
            vval[0] = 0.0;
            vval[1] = basis_triangle(int((j+2)/4),l1,l2);
            vval[2] = 0.0;
            vval[3] = 0.0;
        break;
    case 3:
            vval[0] = 0.0;
            vval[1] = 0.0;
            vval[2] = basis_triangle(int((j+1)/4),l1,l2);
            vval[3] = 0.0;
        break;
    
    }
    return vval;
    delete [] vval;
    
}

double * basis_RT(int jj, double l1, double l2, int deg){
    
    double * vval = new double[2];
    
    if(jj <= (deg+1)*(deg+2)){
        
        if(jj%2 == 0){
            vval[0] = 0.0;
            vval[1] = basis_triangle(int(jj/2),l1,l2);
        }
        else{
            vval[0] = basis_triangle(int((jj+1)/2),l1,l2);
            vval[1] = 0.0;
        }
        
    }
    else{
            vval[0] = basis_triangle(int(jj-(deg+1)*(deg+4)/2),l1,l2)*l1;
            vval[1] = basis_triangle(int(jj-(deg+1)*(deg+4)/2),l1,l2)*l2;
        }
    
    return vval;
    delete [] vval;
}

double * jac_g(int j,double l1,double l2){
    
    double * jacob = new double[8]; // the output would be a 2 by 4 matrix, use the vectorization convention
    switch (j%4){
    
        case 0:
                jacob[0] = 0.0;
                jacob[1] = 0.0;
                jacob[2] = 0.0;
                jacob[3] = 0.0;
                jacob[4] = 0.0;
                jacob[5] = 0.0;
                jacob[6] = DL1_bases_triangle(int(j/4),l1,l2);
                jacob[7] = DL2_bases_triangle(int(j/4),l1,l2);
            break;
            
        case 1:
                jacob[0] = DL1_bases_triangle(int((j+3)/4),l1,l2);
                jacob[1] = DL2_bases_triangle(int((j+3)/4),l1,l2);
                jacob[2] = 0.0;
                jacob[3] = 0.0;
                jacob[4] = 0.0;
                jacob[5] = 0.0;
                jacob[6] = 0.0;
                jacob[7] = 0.0;
            break;
            
        case 2:
                jacob[0] = 0.0;
                jacob[1] = 0.0;
                jacob[2] = DL1_bases_triangle(int((j+2)/4),l1,l2);
                jacob[3] = DL2_bases_triangle(int((j+2)/4),l1,l2);
                jacob[4] = 0.0;
                jacob[5] = 0.0;
                jacob[6] = 0.0;
                jacob[7] = 0.0;
            break;
            
        case 3:
                jacob[0] = 0.0;
                jacob[1] = 0.0;
                jacob[2] = 0.0;
                jacob[3] = 0.0;
                jacob[4] = DL1_bases_triangle(int((j+1)/4),l1,l2);
                jacob[5] = DL2_bases_triangle(int((j+1)/4),l1,l2);
                jacob[6] = 0.0;
                jacob[7] = 0.0;
            break;
    }
    return jacob;
    delete [] jacob;

}

// to be augmented .....

