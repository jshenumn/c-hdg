#ifndef LOCAL_SOLVER_H
#define LOCAL_SOLVER_H
#include <Eigen/Eigen>
using namespace Eigen;
// Local_Mat class contains the local solver contribution to global assemble
class Local_Mat{
public:
        Local_Mat();
       ~Local_Mat();
        Local_Mat & operator= (const Local_Mat & other);
    
        //accessors
        MatrixXd get_KK(){return KK;};
        VectorXd get_BB(){return BB;};
        MatrixXd get_Aq(){return Aq;};
        MatrixXd get_Au(){return Au;};
        VectorXd get_RHSq(){return RHSq;};
        VectorXd get_RHSu(){return RHSu;};
        int get_size(){return dim_m;};
    
        // setters
        void set_KK(MatrixXd &KK);
        void set_BB(VectorXd &BB);
        void set_Aq(MatrixXd &Aq);
        void set_Au(MatrixXd &Au);
        void set_RHSq(VectorXd &RHSq);
        void set_RHSu(VectorXd &RHSu);
    
    
    
private:
    MatrixXd  KK;
    VectorXd  BB;
    MatrixXd  Aq;
    MatrixXd  Au;
    
    VectorXd  RHSq;
    VectorXd  RHSu;
    int dim_m;
};





// volume integral

// compute local integration (q,v)
void compute_qv(MatrixXd &mtx_qv, Element2D & ele, double col_v_triangle[][2], int dim_v, Quad &QT);
// compute local integration (u, div v)
void compute_udivv(MatrixXd &mtx_udivv, Element2D &ele, Edge2D edge_list[], double col_jv_triangle[][4], double col_w_triangle[], int dim_v, int dim_w, Quad &QT);
// compute local integration (f,w)
void compute_fw(VectorXd &mtx_fw, Element2D &ele, Point2D node_list[], double col_w_triangle[], int dim_w , Quad &QT ,std::function<double(double,double)> func);

// face integral
// compute local integration <uhat, mu> on all 3 faces
void compute_uhatuhat(MatrixXd mtx_uhatuhat[], Element2D &ele, Edge2D edge_list[],double col_m_edge[], int dim_m, Quad &QE);
// compute local integration <uhat, vn> on all 3 faces
void compute_uhatvn(MatrixXd mtx_uhatvn[], Element2D &ele, Edge2D edge_list[], double col_v_edge[][6],double col_m_edge[], int dim_v, int dim_m, Quad &QE);
// compute local integration <uh,w> on all 3 faces
void compute_uhuh(MatrixXd &mtx_uhuh, Element2D &ele, Edge2D edge_list[], double col_w_edge[][3], int dim_w, Quad &QE);
// compute local integration <uh,mu> on all 3 faces
void compute_uhuhat(MatrixXd mtx_uhuhat[], Element2D &ele, Edge2D edge_list[], double col_m_edge[], double col_w_edge[][3], int dim_w, int dim_m, Quad &QE);



// Local Solver Matrix
void compute_local_solver_matrix(Element2D &ele, Edge2D edge_list[], Point2D node_list[], std::function<double(double,double)> func, double col_v_triangle[][2], double col_jv_triangle[][4], double col_w_triangle[], double col_m_edge[], double col_w_edge[][3], double col_v_edge[][6], int dim_m, int dim_w, int dim_v, Quad &QT, Quad &QE, Local_Mat & Lmat);


#endif