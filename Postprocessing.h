#include<iostream>
#include<vector>
#include<cmath>
#include<fstream>
#include<sstream>
#include"Basis_function.h"
#include"Physics.h"
#include"Mesh.h"
#include"Collocation.h"
#include"Quadrature.h"
#include"Local_solver.h"
#include"Global_solver.h"
#include"Reconstruction.h"
#include<Eigen/Eigen>
#include<Eigen/Sparse>
#include<time.h>
#include<stdio.h>
#include<math.h>


void postprocessing(VectorXd &solpU, VectorXd &solQ, double &avgU, Element2D& ele, Edge2D edge_list[], int dim_v, int dim_wstar,double col_wstar_triangle[],double col_v_triangle[][2],Quad &QT);

void grad_R2(double gradEle[][2], Element2D &ele, Edge2D edge_list[], int dim_w, Quad &QT);