#include<iostream>
#include<vector>
#include<cmath>
#include"Basis_function.h"
#include"Physics.h"
#include"Mesh.h"
#include"Collocation.h"
#include"Quadrature.h"
#include"Local_solver.h"
#include <Eigen/Eigen> // invoke Eigen routines for matrix

using namespace std;
int main(){

    int dim_m = 2;
    int dim_w = 3;
    int dim_v = 6;

    Quad QE;
    QE.set_edge_quad(4);
    int n_pt = QE.get_n_pt();
    Quad QT;
    QT.set_ele_quad(4);
    int m_pt = QT.get_n_pt();


    // collocation
    double col_m_edge[dim_m*n_pt];
    double col_w_edge[dim_w*n_pt][3];
    double col_v_edge[dim_v*n_pt][6];

    double col_w_triangle[dim_w*m_pt];
    double col_v_triangle[dim_v*m_pt][2];
    double col_jv_triangle[dim_v*m_pt][4];
    double divEle[dim_v*m_pt];

    collocation_edge(dim_m, dim_w, dim_v, col_m_edge, col_w_edge, col_v_edge, QE);
    collocation_triangle(dim_w, dim_v, col_w_triangle, col_jv_triangle, col_v_triangle, QT);

    // initializate mesh

    string path_ele = "./MESH/MESH.2.ele";
    string path_edge = "./MESH/MESH.2.edge";
    string path_node = "./MESH/MESH.2.node";


    // read element and initialize element objects
    vector<Row> element_table = mesh_read(path_ele);
    /*
     ------------- The format of .ele file -------------------
     # element id      # connectivity  1 by 3 vector  # element type 0 or 1
     ! first row contains data information.
     */

    // read edge and initialize edge objects
    vector<Row> edge_table = mesh_read(path_edge);
    /*
     ------------- The format of .edge file -------------------
     # edge id      # vert1   #vert2  # element type 0(boundary) or 1-4
     ! first row contains data information.
     */

    // read edge and initialize edge objects
    vector<Row> node_table = mesh_read(path_node);
    /*
     ------------- The format of .edge file -------------------
     # point id       x   y  # element type 0(boundary) or 1-4
     ! first row contains data information.
     */





    Element2D *element_list = new Element2D[element_table.size()];

    for (int r = 1; r < element_table.size() ; ++r) {
        // each row read connectivity into an array
        int cnt[3];

        for (int i = 1; i < 4; ++i){
            cnt[i-1] = element_table[r][i];
        }

        element_list[r].set_connectivity(cnt); // initialize cnt

        // compute area, normal requires vert_coord array
        double vert_coord[6];

        for (int j = 0; j<3; ++j) {
            vert_coord[2*j] = node_table[cnt[j]][1];
            vert_coord[2*j+1] = node_table[cnt[j]][2];
        }

        element_list[r].compute_area(vert_coord); // compute area
        element_list[r].compute_normal(vert_coord); // compute normal

        // set edge global number requires edge local number, this is done by
        // inspect through edge_table and find corresponding edge local position
        for (int k = 1; k < edge_table.size(); ++k) { //loop over all edges
            int varry[2];
            // gather vertices index
            for (int ii = 1; ii < 3; ++ii){
                varry[ii-1] = edge_table[k][ii];
            }

            int edg_ln = element_list[r].check_edge_in_element(varry[0], varry[1]);

            if (edg_ln != -1){ // if kth edge in r-th element, set edge info
                element_list[r].set_edg_gn(k,edg_ln);
            }

        } // edge loop
    }// element loop

    Point2D * node_list = new Point2D[node_table.size()];

    for (int r = 1; r < node_table.size() ; ++r) {
        // each row read vertices pari into an array
        double coord[2];

        for (int i = 1; i < 3; ++i){
            coord[i-1] = node_table[r][i];
        }

        node_list[r].set_coord(coord); // initialize element

    }


    Edge2D *edge_list = new Edge2D[edge_table.size()];

    for (int r = 1; r < edge_table.size() ; ++r) {
        // each row read vertices pari into an array
        int vt[2];
        int type = 0;

        for (int i = 1; i < 3; ++i){
            vt[i-1] = edge_table[r][i];
        }
        type = edge_table[r][3];
        edge_list[r].set_type(type);
        edge_list[r].set_vert(vt); // initialize vert
        edge_list[r].set_length(node_list[vt[0]],node_list[vt[1]]); // set_length
    }
        // local solver computing initialization
//        MatrixXd mtx_qv(dim_v,dim_v);
//        mtx_qv.setZero();
//    
//
//        MatrixXd mtx_udivv(dim_v,dim_w);
//        mtx_udivv.setZero();
//    
//    
//        VectorXd mtx_fw(dim_w);
//        mtx_fw.setZero();
//
//    
//        MatrixXd mtx_uhatuhat[3];
//        for(int i = 0; i < 3; i++){
//            mtx_uhatuhat[i] = MatrixXd(dim_m,dim_m);
//            mtx_uhatuhat[i].setZero();
//        }
//
//        MatrixXd mtx_uhatvn[3];
//        for(int i = 0; i < 3; i++){
//            mtx_uhatvn[i] = MatrixXd(dim_v,dim_m);
//            mtx_uhatvn[i].setZero();
//        }
//
//        MatrixXd mtx_uhuh(dim_w,dim_w);
//        mtx_uhuh.setZero();
//
//
//        MatrixXd mtx_uhuhat[3];
//        for(int i = 0; i < 3; i++){
//            mtx_uhuhat[i] = MatrixXd(dim_w,dim_m);
//            mtx_uhuhat[i].setZero();
//        }
    


//        // invoke functions
//        compute_qv(mtx_qv, element_list[1], col_v_triangle, dim_v, QT);
//        compute_udivv(mtx_udivv, element_list[1], edge_list, col_jv_triangle, col_w_triangle, dim_v, dim_w, QT);
//        compute_fw(mtx_fw, element_list[1], node_list, col_w_triangle, dim_w, QT, source_f);
//        compute_uhatuhat(mtx_uhatuhat, element_list[1], edge_list, col_m_edge, dim_m, QE);
//        compute_uhatvn(mtx_uhatvn, element_list[1], edge_list, col_v_edge, col_m_edge, dim_v, dim_m, QE);
//        compute_uhuh(mtx_uhuh, element_list[1], edge_list, col_w_edge, dim_w, QE);
//        compute_uhuhat(mtx_uhuhat, element_list[1], edge_list, col_m_edge, col_w_edge, dim_w, dim_m, QE);

        // debuggers
    
        //cout << mtx_qv <<endl;
    
        //cout << mtx_udivv << endl;
    
        //cout << mtx_fw << endl;
        /*
        for (int i = 0; i < 3; ++i) {
            cout << "Edge" << i+1 << endl;
            cout << mtx_uhatvn[i] << endl;
        }
        */
    
        /*
         for (int i = 0; i < 3; ++i) {
         cout << "Edge" << i+1 << endl;
         cout << mtx_uhatuhat[i] << endl;
         }
         */
        //cout << mtx_uhuh << endl;
        /*
        for (int i = 0; i < 3; ++i) {
            cout << "Edge" << i+1 << endl;
            cout << mtx_uhuhat[i] << endl;
        }
        */
    
    
//    // Local matrix A construction
//    MatrixXd A(dim_v+dim_w,dim_v+dim_w);
//    MatrixXd matzero(dim_v,dim_w);
//    matzero.setZero();
//    A.setZero();
//    
//    // assign local matrices
//    A.topLeftCorner(dim_v,dim_v) = mtx_qv;
//    A.topRightCorner(dim_v,dim_w) = matzero - mtx_udivv;
//    A.bottomLeftCorner(dim_w,dim_v) = mtx_udivv.transpose();
//    A.bottomRightCorner(dim_w,dim_w) = mtx_uhuh;
//    
//    // Local RHS construction (could be smarter, need to rewrite)
//    VectorXd fvec(dim_v + dim_w);
//    for (int  i = 0; i < dim_v; ++i) {
//        fvec(i) = 0.0;
//    }
//    
//    for (int  j = 0; j < dim_w; ++j) {
//        fvec(j + dim_v) = 0.0 - mtx_fw(j);
//    }
//    
//    // Local Uhat matrix construction
//    MatrixXd Uhat(dim_v + dim_w, 3*dim_m);
//    Uhat.block(0,0,dim_v,dim_m) = - mtx_uhatvn[0];
//    Uhat.block(0,dim_m,dim_v,dim_m) = - mtx_uhatvn[1];
//    Uhat.block(0,dim_m*2,dim_v,dim_m) = - mtx_uhatvn[2];
//    Uhat.block(dim_v,0,dim_w,dim_m) = mtx_uhuhat[0];
//    Uhat.block(dim_v,dim_m,dim_w,dim_m) = mtx_uhuhat[1];
//    Uhat.block(dim_v,dim_m*2,dim_w,dim_m) = mtx_uhuhat[2];
//    
//    MatrixXd Ae(dim_v + dim_w, 3*dim_m);
//    VectorXd RHSe(dim_v + dim_w);
//
//    Ae = A.inverse() * Uhat;
//    RHSe = A.inverse() *fvec;
//    
//    MatrixXd Aq(dim_v, 3*dim_m);
//    MatrixXd Au(dim_w, 3*dim_m);
//    VectorXd RHSq(dim_v);
//    VectorXd RHSu(dim_w);
//    
//    Aq = Ae.block(0,0,dim_v,3*dim_m);
//    Au = Ae.block(dim_v,0,dim_w,3*dim_m);
//    
//    RHSq = RHSe.segment(0,dim_v);
//    RHSu = RHSe.segment(dim_v,dim_w);
//    
//    MatrixXd KK(3*dim_m,3*dim_m);
//    VectorXd BB(3*dim_m);
//    
//    MatrixXd proxy1(dim_v,3*dim_m);
//    MatrixXd proxy2(dim_w,3*dim_m);
//    proxy1= Uhat.block(0,0,dim_v,3*dim_m);
//    proxy2= Uhat.block(dim_v,0,dim_w,3*dim_m);
//    
//    KK = -proxy1.transpose() *  Aq + proxy2.transpose() * Au;
//    BB = -proxy1.transpose() *  RHSq + proxy2.transpose() * RHSu;
//    
//    KK.block(0,0,dim_m,dim_m) = KK.block(0,0,dim_m,dim_m) - mtx_uhatuhat[0];
//    KK.block(dim_m,dim_m,dim_m,dim_m) = KK.block(dim_m,dim_m,dim_m,dim_m) - mtx_uhatuhat[1];
//    KK.block(2*dim_m,2*dim_m,dim_m,dim_m) = KK.block(2*dim_m,2*dim_m,dim_m,dim_m) - mtx_uhatuhat[2];
//    
//    cout << KK << endl;

    Local_Mat Lmat(dim_m,dim_v,dim_w);
    
    compute_local_solver_matrix(element_list[8], edge_list, node_list, source_f, col_v_triangle, col_jv_triangle, col_w_triangle, col_m_edge, col_w_edge,col_v_edge, dim_m, dim_w, dim_v, QT, QE, Lmat);
    
    cout << Lmat.get_KK() << endl;

    

}