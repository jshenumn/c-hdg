// Define physics data and exact solutions
 double source_f( double x,  double y);
 double boundary_g( double x,  double y, int marker);
 double exactU( double x,  double y);
 double * exactQ( double x,  double y);