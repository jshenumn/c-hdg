#include <Eigen/Eigen>
#include <Eigen/Sparse>
#include <vector>
using namespace Eigen;
using namespace std;

typedef SparseMatrix<double> SpMat; // sparse matrix of double type

// function prototype
void Global_Assemble(SpMat &A, VectorXd &rhs, Local_Mat Lmat_list[], int Mymap[][6], int n_ele, int n_edge,Element2D element_list[],int dim_m);

void dirichletBC(SpMat &A, VectorXd &rhs,int Mymap[][6],vector<int> DBCarray, Edge2D edge_list[], Point2D node_list[], double col_m_edge[], int dim_m, function<double(double,double,int)> func, Quad &QE);

void insertmap(int Mymap[][6], int indx, int value);